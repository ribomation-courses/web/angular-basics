import { Component } from '@angular/core';
import {environment} from "../environments/environment";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'config dependent images';
  readonly imageUrl = environment.imageUrl;
  readonly productionMode = environment.production;
}
