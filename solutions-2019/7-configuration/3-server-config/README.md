# Config Dependent Images

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.1.

# How to Launch the DEV Server

## Dev Config

    npm run start-dev

Then, browse to [`http://localhost:4200/`](http://localhost:4200/)

## Prod Config

    npm run start-prod

Then, browse to [`http://localhost:4300/`](http://localhost:4300/)

# How to Launch the PROD Server

## First, install the web server

    npm install --save-dev local-web-server

## Then, build the app

    npm run build

## Finally, launch the production web app

    npm run launch

