import { Injectable } from '@angular/core';
import { Product } from './product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private _products: Product[];

  constructor() {
    const price = () => Math.floor(10 + 100 * Math.random());

    this._products = [
      new Product('Köttgryta', price(), 'http://lorempixel.com/100/100/food/1/'),
      new Product('Grönsaker', price(), 'http://lorempixel.com/100/100/food/5/'),
      new Product('Bröd'     , price(), 'http://lorempixel.com/100/100/food/7/'),
      new Product('Sushi'    , price(), 'http://lorempixel.com/100/100/food/8/'),
    ];
  }

  get all(): Product[] { return this._products; }

}
