import {Account} from './account';
import {Generator} from './generator'

const accounts: Account[] = [];
const N = 100;

for (let k=0; k<N; ++k) {
    accounts.push(Generator.mk());
}

for (let k = 0; k < accounts.length; ++k) {
    console.log(accounts[k].toString());
}
