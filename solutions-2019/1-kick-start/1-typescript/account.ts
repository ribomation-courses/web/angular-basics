export  class Account {
    constructor(
        private _accno: string,
        private _balance: number,
        private _credit: boolean,
        private _created: Date,
        private _owner: string
    ) {  }

    toString():string {
        return `Account[${this.accno}, ${this.balance} kr, ${this.owner}]`;
    }

    get accno(): string { return this._accno.toUpperCase(); }
    get balance(): number { return this._balance; }
    get credit(): boolean { return this._credit; }
    get created(): Date { return this._created; }
    get owner(): string { return this._owner; }
}

