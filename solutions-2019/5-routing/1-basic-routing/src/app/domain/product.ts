export class Product {
    constructor(
        public readonly id: number,
        public readonly name: string,
        public readonly price: number,
        public readonly description: string
    ) { }
}

export class ProductGenerator {
    mk(): Product {
        const id = Math.floor(999 * Math.random());
        const name = this.pick(this.names);
        const price = 10 + Math.random() * 100;
        const desc = this.sentence();
        return new Product(id, name, price, desc);
    }

    lst(n: number): Product[] {
        const result: Product[] = [];
        while (n-- > 0) {
            result.push(this.mk());
        }
        return result;
    }

    private names = [
        'apple', 'banana', 'coco nut', 'date plum'
    ];

    private words = [
        'bla', 'yada', 'foobar', 'hepp', 'tjolla hopp', 'ipsum'
    ];

    private sentence(): string {
        let result = '';
        let n = 20;
        while (n-- > 0) {
            result += this.pick(this.words) + ' ';
        }
        return result;
    }

    private pick(arr: string[]): string {
        const idx = Math.floor(arr.length * Math.random());
        return arr[idx];
    }
}
