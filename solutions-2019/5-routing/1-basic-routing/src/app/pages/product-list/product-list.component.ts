import { Component } from '@angular/core';
import { ProductRepoService } from 'src/app/services/product-repo.service';
import { Product } from 'src/app/domain/product';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent {
  constructor(
    public prodSvc: ProductRepoService,
    private router: Router
  ) { }

  show(item: Product) {
    this.router.navigate(['/products/show', item.id]);
  }
}
