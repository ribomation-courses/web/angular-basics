import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductRepoService } from 'src/app/services/product-repo.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/domain/product';
import { Subscription } from 'rxjs';
import { map, filter, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit, OnDestroy {
  id:number;
  product: Product;
  sub: Subscription;

  constructor(
    private prodSvc: ProductRepoService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.id = parseInt(this.route.snapshot.paramMap.get('id') );
    this.product = this.prodSvc.findById(this.id);

    this.sub = this.route.params
      .pipe(
        map(p => p.id),
        filter(id => id != undefined)
      )
      .subscribe(id => {
        this.product = this.prodSvc.findById(id);
      });
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

}
