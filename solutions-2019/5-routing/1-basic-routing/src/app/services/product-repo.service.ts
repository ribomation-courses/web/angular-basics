import { Injectable } from '@angular/core';
import { Product, ProductGenerator } from '../domain/product';

@Injectable({
  providedIn: 'root'
})
export class ProductRepoService {
  private _items: Product[] = [];

  constructor() {
    const g = new ProductGenerator();
    this._items = g.lst(10);
  }

  get items(): Product[] {
    return this._items;
  }

  findById(id: number): Product {
    return this.items.find(p => p.id === +id);
  }
}
