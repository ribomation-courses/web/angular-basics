import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-template-oriented',
  templateUrl: './template-oriented.component.html',
  styleUrls: ['./template-oriented.component.scss']
})
export class TemplateOrientedComponent implements OnInit {
  @Input('init') init: any;
  @Output('submit') submitEmitter = new EventEmitter<any>();
  @Output('cancel') cancelEmitter = new EventEmitter<void>();

  @ViewChild('form', { static: false }) form: FormGroup;
  v: any;

  ngOnInit() {
    this.v = this.init;
  }

  onSubmit() {
    this.submitEmitter.emit(this.form.value);
  }

  onCancel() {
    this.cancelEmitter.emit();
  }

}
