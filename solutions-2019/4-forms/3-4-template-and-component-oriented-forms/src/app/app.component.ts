import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'registration form';

  user: any = {
    name: 'Per Silja', age: 42
  };

  onSubmit(payload: any) {
    console.info('[submit]', 'payload:', payload);

    if (payload.name && payload.age) {
      this.user.name = payload.name;
      this.user.age = payload.age;
    }
  }

  onCancel() {
    console.info('[cancel]');
  }
}
