import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-component-oriented',
  templateUrl: './component-oriented.component.html',
  styleUrls: ['./component-oriented.component.scss']
})
export class ComponentOrientedComponent implements OnInit {
  @Input('init') init: any;
  @Output('submit') submitEmitter = new EventEmitter<any>();
  @Output('cancel') cancelEmitter = new EventEmitter<void>();

  form: FormGroup;

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(this.init.name, [
        Validators.required, Validators.minLength(3), Validators.pattern(/[a-zA-Z ]+/)
      ]),
      age: new FormControl(this.init.age, [
        Validators.required, Validators.min(18), Validators.max(70)
      ]),
    });
  }

  get f(): any {
    return this.form.controls;
  }

  onSubmit() {
    this.submitEmitter.emit(this.form.value);
  }

  onCancel() {
    this.cancelEmitter.emit();
  }
}
