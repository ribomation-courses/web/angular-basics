import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'date / range';
  date: Date;
  offset: number;

  ngOnInit() {
    this.onDateChanged(new Date().toLocaleDateString());
    this.offset = 1;
  }

  nextDate(date: Date, offset: number): Date {
    const DAY_MS = 24 * 3600 * 1000;
    return new Date(date.getTime() + offset * DAY_MS);
  }

  onDateChanged(dateTxt: string) {
    const date = new Date(dateTxt);
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);
    this.date = date;
  }

}
