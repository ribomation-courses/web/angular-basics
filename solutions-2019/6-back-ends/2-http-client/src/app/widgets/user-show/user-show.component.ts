import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { User } from 'src/app/domain/user';

@Component({
  selector: 'app-user-show',
  templateUrl: './user-show.component.html',
  styleUrls: ['./user-show.component.scss']
})
export class UserShowComponent implements OnInit {
  user$: Observable<User>;

  constructor(
    private userSvc: UsersService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.user$ = this.route.params.pipe(
      map(p => p.id),
      tap(id => console.debug('** id', id)),
      switchMap(id => this.userSvc.findById(id)),
      tap(obj => console.debug('** obj', obj))
    );

  }

}
