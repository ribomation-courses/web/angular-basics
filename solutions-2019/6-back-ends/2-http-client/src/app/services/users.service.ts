import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../domain/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  readonly baseUrl = 'http://localhost:3000/users';

  constructor(private http: HttpClient) { }

  findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl);
  }

  findById(id:number): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/${id}`);
  }

}
