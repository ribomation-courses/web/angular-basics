import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { User } from '../domain/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  readonly baseUrl = 'http://localhost:3000/users';
  readonly authUrl = 'http://localhost:3000/auth/login';
  private authToken: string;

  constructor(private http: HttpClient) { }

  findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl);
  }

  findById(id: number): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/${id}`);
  }

  login():Promise<boolean> {
    const payload = { username: 'nisse', password: 'hult' };

    return this.http
    .post(this.authUrl, payload, {responseType: 'text'})
      .pipe(
        take(1)
      )
      .toPromise()
      .then(jwt => {
        this.authToken = jwt;
        console.info('authenticated', jwt);
        return Promise.resolve(true);
      })
      .catch(err => {
        console.error('auth failed', err);
        return Promise.reject(false);
      });
  }

  logout() {
    this.authToken = undefined;
  }

  isAuthenticated():boolean {
    return (this.authToken !== undefined) && (typeof this.authToken === 'string');
  }

}
