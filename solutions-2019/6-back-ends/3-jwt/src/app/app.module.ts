import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Route, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { UserListComponent } from './widgets/user-list/user-list.component';
import { UserShowComponent } from './widgets/user-show/user-show.component';

export const routes: Route[] = [
  {
    path: 'users', component: UserListComponent, children: [
      { path: 'show/:id', component: UserShowComponent },
      { path: '', redirectTo: 'show', pathMatch: 'full' }
    ]
  },
  {
    path: '', redirectTo: 'users', pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserShowComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
