import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { User } from 'src/app/domain/user';

@Component({
  selector: 'app-user-show',
  templateUrl: './user-show.component.html',
  styleUrls: ['./user-show.component.scss']
})
export class UserShowComponent implements OnInit {
  user$: Observable<User>;
  message: string;

  constructor(
    private userSvc: UsersService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    if (this.userSvc.isAuthenticated() === false) {
      this.message = 'Logging in ...';

      this.userSvc.login()
        .then(_ => {
          this.user$ = this.route.params
            .pipe(
              map(p => p.id),
              tap(id => console.debug('** id', id)),
              switchMap(id => this.userSvc.findById(id)),
              tap(obj => console.debug('** obj', obj))
            );

          this.message = undefined;
        })
        .catch(_ => {
          console.error('cannot login');
          this.message = 'Failed to login';
        });
    }

  }

}
