import { Component } from '@angular/core';
import { User } from './domain/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'editable text demo';
  user: User = new User('Ana Gram', 42, undefined);

  onUpdate(key:string, value:any) {
    console.info(`** update: ${key}='${value}'`);
  }
}
