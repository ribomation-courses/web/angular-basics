import { Component } from '@angular/core';
import { MultiplyService } from './services/multiply.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'multiply';
  value: number = 5;

  constructor(public mulSvc: MultiplyService){}
  
}
