import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MultiplyService {
  private _factor: number = 7;

  constructor() { }

  compute(arg: number): number {
    return arg * this._factor;
  }

  set factor(n: number|string) {
    console.debug('***', typeof n, n);
    this._factor = +n;
  }
}
