import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User}       from './user';
import {map}       from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  readonly baseUrl: string = 'http://localhost:3000/users/';

  constructor(private http: HttpClient) {
  }

  findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl);
  }

  findAny(): Observable<User> {
    return this.http.get<User[]>(this.baseUrl)
      .pipe(
        map(objs => (objs && objs.length > 0) ? objs[0]: undefined)
      );
  }

  create(user: User): Observable<User> {
    return this.http.post<User>(this.baseUrl, user);
  }

  update(user: User): Observable<User> {
    return this.http.put<User>(this.baseUrl + user.id, user);
  }

  remove(user: User): Observable<any> {
    return this.http.delete<User>(this.baseUrl + user.id);
  }

}
