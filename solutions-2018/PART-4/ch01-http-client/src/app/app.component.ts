import {Component, OnInit} from '@angular/core';
import {UsersService}      from './users.service';
import {Observable}        from 'rxjs';
import {User}              from './user';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent implements OnInit {
  users$: Observable<User[]>;
  user$: Observable<User>;
  user: User;

  constructor(private usersSvc: UsersService) {
  }

  ngOnInit(): void {
    this.users$ = this.usersSvc.findAll();
    this.user$  = this.usersSvc.findAny();
  }

  edit(user: User) {
    this.user = user;
  }

  create() {
    this.user = {name: '', age: 0};
  }

  save() {
    if (this.user) {
      const done = () => {
        this.user = undefined;
        this.ngOnInit();
      };

      if (this.user.id) {
        this.usersSvc.update(this.user)
          .subscribe(done);
      } else {
        this.usersSvc.create({name: this.user.name, age: this.user.age})
          .subscribe(done);
      }
    }
  }

  delete(user: User) {
    this.usersSvc.remove(user).subscribe(_ => {
      this.ngOnInit();
    });
  }

}
