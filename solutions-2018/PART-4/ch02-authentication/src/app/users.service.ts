import {Injectable}              from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError}  from 'rxjs';
import {User}                    from './user';
import {catchError, map}         from 'rxjs/operators';
import {AuthService}             from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  readonly baseUrl: string = 'http://localhost:3000/users/';

  constructor(private http: HttpClient, private authSvc: AuthService) {
  }

  findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl);
  }

  findAny(): Observable<User> {
    return this.http.get<User[]>(this.baseUrl)
      .pipe(
        map(objs => (objs && objs.length > 0) ? objs[0] : undefined)
      );
  }

  create(user: User): Observable<User> {
    const headers = new HttpHeaders()
      .set('Auth-Token', this.authSvc.token || '***')
    ;
    return this.http
      .post<User>(this.baseUrl, user, {headers})
      .pipe(
        catchError(err => {
          console.warn('[users svc]', 'create()', 'failure', err.message || err);
          return throwError(err)
        })
      )
      ;
  }

  update(user: User): Observable<User> {
    const headers = new HttpHeaders()
      .set('Auth-Token', this.authSvc.token || '***')
    ;
    return this.http.put<User>(this.baseUrl + user.id, user, {headers});
  }

  remove(user: User): Observable<any> {
    const headers = new HttpHeaders()
      .set('Auth-Token', this.authSvc.token || '***')
    ;
    return this.http.delete<User>(this.baseUrl + user.id, {headers});
  }

}
