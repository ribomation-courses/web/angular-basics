import {Component, OnInit} from '@angular/core';
import {UsersService}      from './users.service';
import {Observable}        from 'rxjs';
import {User}              from './user';
import {AuthService}       from './auth.service';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent implements OnInit {
  users$: Observable<User[]>;
  user$: Observable<User>;
  user: User;
  credentials    = {
    username: '', password: ''
  };
  errmsg: string = undefined;

  constructor(private usersSvc: UsersService, private authSvc: AuthService) {
  }

  get auth(): AuthService {
    return this.authSvc;
  }

  ngOnInit(): void {
    this.users$ = this.usersSvc.findAll();
    this.user$  = this.usersSvc.findAny();
  }

  edit(user: User) {
    this.user = user;
  }

  create() {
    this.user = {name: '', age: 0};
  }

  save() {
    if (this.user) {
      this.errmsg = undefined;

      const success = () => {
        this.user   = undefined;
        this.errmsg = undefined;
        this.ngOnInit();
      };

      const failure = (err) => {
        console.warn('[app]', 'save()', 'failure', err.message || err);
        this.errmsg = err ? err.message : 'FAILURE';
      };

      if (this.user.id) {
        this.usersSvc.update(this.user)
          .subscribe(success, failure);
      } else {
        this.usersSvc.create({name: this.user.name, age: this.user.age})
          .subscribe(success, failure);
      }
    }
  }

  delete(user: User) {
    this.errmsg = undefined;
    this.usersSvc.remove(user)
      .subscribe(
        () => {
          this.ngOnInit();
        },
        (err) => {
          console.warn('[app]', 'save()', 'failure', err.message || err);
          this.errmsg = err ? err.message : 'FAILURE';
        }
      );
  }

  login() {
    this.errmsg = undefined;
    this.authSvc
      .login(this.credentials.username, this.credentials.password)
      .subscribe(
        token => {
          console.info('login ok:', token);
        },
        err => {
          console.info('login failure:', err);
          this.errmsg = err ? err.message : 'FAILURE';
        },
        () => {
          this.credentials.password = '';
        }
      );
  }

  logout() {
    this.authSvc.logout();
  }


}
