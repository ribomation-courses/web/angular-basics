import {Component}              from '@angular/core';
import {PriceCalculatorService} from './price-calculator.service';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.scss']
})
export class AppComponent {
  products = [
    {
      name: 'Apple', count: 1, price: 12.50
    },
    {
      name: 'Banana', count: 3, price: 3.75
    },
    {
      name: 'Lemon', count: 1, price: 8.25
    },
    {
      name: 'Orange', count: 4, price: 8.25
    },
  ];

  constructor(
    private priceSvc: PriceCalculatorService
  ) {
  }

  get sumAmount(): number {
    return this.products
      .map(p => p.price * p.count)
      .reduce((sum, p) => sum + p, 0);
  }

  get vatAmount(): number {
    return this.sumAmount * this.priceSvc.VAT / 100;
  }

  get shippingAmount(): number {
    return this.priceSvc.shipping;
  }

  get totalAmount(): number {
    return this.priceSvc.totalPrice(this.sumAmount);
  }

}
