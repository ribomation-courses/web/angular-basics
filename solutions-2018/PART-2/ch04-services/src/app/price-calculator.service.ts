import {Inject, Injectable}         from '@angular/core';
import {SHIPPING_COST, VAT_PERCENT} from './tokens';

@Injectable({
  providedIn: 'root'
})
export class PriceCalculatorService {
  constructor(
    @Inject(VAT_PERCENT) private vatPercent: number,
    @Inject(SHIPPING_COST) private shippingCost: number,
  ) {
  }

  get VAT(): number {
    return this.vatPercent;
  }

  get shipping(): number {
    return this.shippingCost;
  }

  totalPrice(amount: number): number {
    return amount * (1 + this.VAT / 100) + this.shipping;
  }

}
