import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
}                    from '@angular/core';
import {Result}      from '../result.model';
import {EvalService} from '../eval.service';

@Component({
  selector:    'app-results',
  templateUrl: './results.component.html',
  styleUrls:   ['./results.component.css']
})
export class ResultsComponent implements OnInit, OnChanges {
  @Input('lhs') lhsIn: number = 30;
  @Input('rhs') rhsIn: number = 35;
  results: Result[]           = [];

  constructor(private evalSvc: EvalService) {
  }

  ngOnInit() {
    this.results = this.evalSvc.compute(this.lhsIn, this.rhsIn);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.info('[results]', this.lhsIn, this.rhsIn);
    this.results = this.evalSvc.compute(this.lhsIn, this.rhsIn);
  }

}
