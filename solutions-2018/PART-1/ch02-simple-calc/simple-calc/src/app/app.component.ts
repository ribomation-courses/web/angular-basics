import { Component } from '@angular/core';
import { Result } from './result.model';
import { OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {
  lhs: number = 40;
  rhs: number = 10;
  results: Result[] = [];

  ngOnInit() {
    this.compute();
  }

  compute(): void {
    this.results = [];
    this.results.push(this.add(this.lhs, this.rhs));
    this.results.push(this.mul(this.lhs, this.rhs));
    this.results.push(this.sub(this.lhs, this.rhs));
    this.results.push(this.div(this.lhs, this.rhs));
  }

  private add(lhs: number, rhs: number): Result {
    return new Result("addition", '+', lhs, rhs, lhs + rhs);
  }

  private mul(lhs: number, rhs: number): Result {
    return new Result("multiplication", '*', lhs, rhs, lhs * rhs);
  }

  private sub(lhs: number, rhs: number): Result {
    return new Result("subtraction", '-', lhs, rhs, lhs - rhs);
  }

  private div(lhs: number, rhs: number): Result {
    return new Result("division", '/', lhs, rhs, lhs / rhs);
  }
}
