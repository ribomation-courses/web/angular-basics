
export class Book {
	constructor(
		private _title: string,
		private _author: string,
		private _price: number
	) { }

	get title(): string {
		return this._title.toUpperCase();
	}

	get author(): string {
		return this._author.toUpperCase();
	}

	get price(): number {
		return 1.25 * this._price;
	}
}

const book:Book = new Book("Angular Primer", "Justin Time", 200);

console.info(`Book: ${book.title} by ${book.author}. Price: ${book.price} kr`);
