const G = require('../index');

describe('simple test suite', () => {
    it('should return a greeting, when given a name', () => {
        expect(G.greeting('Jens')).toBe('Hello Jens!');
    });

    it('should return "Hello !", when name is absent', () => {
        expect(G.greeting()).toBe('Hello !');
    });
});

