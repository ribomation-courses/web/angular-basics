import {Component, OnInit} from '@angular/core';

@Component({
  selector:    'app-not-found',
  templateUrl: './not-found.component.html',
  styles:   []
})
export class NotFoundComponent  {
  image404: string = '/assets/404.png';
}
