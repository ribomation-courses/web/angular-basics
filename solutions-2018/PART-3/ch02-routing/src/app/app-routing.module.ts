import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotFoundComponent}      from './not-found/not-found.component';
import {WelcomeComponent}       from './welcome/welcome.component';
import {ProductListComponent}   from './product-list/product-list.component';
import {ProductDetailComponent} from './product-detail/product-detail.component';

const routes: Routes = [
  {path: 'welcome', component: WelcomeComponent},
  {
    path: 'products', component: ProductListComponent,
    children: [
      {path: 'detail', component: ProductDetailComponent},
    ]
  },
  {path: '', redirectTo: '/welcome', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
