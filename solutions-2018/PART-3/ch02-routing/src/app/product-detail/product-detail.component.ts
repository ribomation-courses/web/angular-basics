import {Component, OnInit} from '@angular/core';
import {map}               from 'rxjs/operators';
import {Book}              from '../book';
import {ActivatedRoute}    from '@angular/router';
import {BookRepo}          from '../book-repo';

@Component({
  selector:    'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls:   ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  book: Book;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params
      .pipe(
        map(p => p.id),
        map(id => BookRepo.book(id))
      )
      .subscribe(b => this.book = b);
  }

}
