import {Injectable}           from '@angular/core';
import {HttpClient}           from '@angular/common/http';
import {Observable, of}       from 'rxjs';
import {map, catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  readonly baseUrl: string  = 'http://localhost:3000/auth/';
  private _token: string    = undefined;
  private _username: string = undefined;

  constructor(private http: HttpClient) {}

  get isAuthenticated(): boolean {
    return this._token !== undefined;
  }

  get token(): string {
    return this._token;
  }

  get user(): string {
    return this._username;
  }

  login(username: string, password: string): Observable<boolean> {
    const credentials = {username, password};
    const options     = {observe: 'body'/*, responseType: 'text'*/};
    console.info('credentials', credentials);

    return this.http
      .post(this.baseUrl + 'login', credentials)
      .pipe(
        map((token: any) => {
          if (token) {
            console.info('logged on', token);
            this._token    = token.token;
            this._username = username;
            // sessionStorage.setItem('jwt', JSON.stringify({
            //   token:this._token, username: this._username
            // }));
            return true;
          } else {
            console.info('failed');
            this.logout();
            return false;
          }
        }),
        catchError(err => {
          console.info('failed', err);
          return of(false);
        })
      );
  }

  logout() {
    this._token    = undefined;
    this._username = undefined;
    // sessionStorage.removeItem('jwt');
  }

}
