import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User}       from './user';
import {map, take}  from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  readonly baseUrl: string = 'http://localhost:3000/users/';

  constructor(private http: HttpClient) {
  }

  findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl);
  }

  findById(id: number): Observable<User> {
    return this.http
      .get<User>(this.baseUrl + id)
      .pipe(
        take(1)
      );
  }

  findAny(): Observable<User> {
    return this.findAll()
      .pipe(
        map((objs: User[]) => {
          if (objs && objs.length > 0) {
            const randValue = new Uint32Array(1);
            window.crypto.getRandomValues(randValue);

            const idx = randValue[0] % objs.length;
            return objs[idx];
          } else {
            return undefined;
          }
        })
      );
  }

  create(user: User): Observable<User> {
    return this.http.post<User>(this.baseUrl, user);
  }

  update(user: User): Observable<User> {
    return this.http.put<User>(this.baseUrl + user.id, user);
  }

  remove(user: User): Observable<any> {
    return this.http.delete<User>(this.baseUrl + user.id);
  }

}
