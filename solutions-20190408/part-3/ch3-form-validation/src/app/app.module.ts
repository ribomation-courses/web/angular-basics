import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent }                   from './app.component';
import { TemplateOrientedComponent }      from './template-oriented/template-oriented.component';
import { ComponentOrientedComponent }     from './component-oriented/component-oriented.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TemplateOrientedComponent,
    ComponentOrientedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
