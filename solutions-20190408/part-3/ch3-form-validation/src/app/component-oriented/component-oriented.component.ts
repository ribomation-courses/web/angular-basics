import {Component, OnInit}                  from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector:    'app-component-oriented',
  templateUrl: './component-oriented.component.html',
  styleUrls:   ['./component-oriented.component.css']
})
export class ComponentOrientedComponent implements OnInit {
  form: FormGroup;

  ngOnInit() {
    this.form = new FormGroup({
      name:    new FormControl('', [Validators.required, Validators.minLength(5)]),
      age:     new FormControl('', [Validators.min(18), Validators.max(100)]),
      created: new FormControl('', [Validators.required]),
    });
  }

  get fields(): any {
    return this.form.controls;
  }

  onSubmit() {
    alert(`User: ${JSON.stringify(this.form.value)}`);
    this.form.reset();
  }
}
