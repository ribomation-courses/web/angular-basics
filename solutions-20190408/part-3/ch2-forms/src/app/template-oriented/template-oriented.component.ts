import {Component, ViewChild} from '@angular/core';
import {FormGroup}            from '@angular/forms';
import {User}                 from '../domain/user';

@Component({
  selector:    'app-template-oriented',
  templateUrl: './template-oriented.component.html',
  styleUrls:   ['./template-oriented.component.css']
})
export class TemplateOrientedComponent {
  @ViewChild('f') form: FormGroup;
  user: User = new User('', 0);

  onSubmit() {
    alert(`User: ${JSON.stringify(this.user)}`);
    this.form.reset();
  }
}
