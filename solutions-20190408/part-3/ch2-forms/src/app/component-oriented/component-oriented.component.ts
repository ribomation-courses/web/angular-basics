import {Component, OnInit}      from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector:    'app-component-oriented',
  templateUrl: './component-oriented.component.html',
  styleUrls:   ['./component-oriented.component.css']
})
export class ComponentOrientedComponent implements OnInit {
  form: FormGroup;

  ngOnInit() {
    this.form = new FormGroup({
      name:    new FormControl(''),
      age:     new FormControl(''),
      created: new FormControl(''),
    });
  }

  get fields(): any {
    return this.form.controls;
  }

  onSubmit() {
    alert(`User: ${JSON.stringify(this.form.value)}`);
    this.form.reset();
  }
}
