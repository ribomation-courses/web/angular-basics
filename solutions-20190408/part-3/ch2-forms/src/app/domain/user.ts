export class User {
  constructor(
    public  name: string,
    public  age: number,
    public  created?: Date)
  {
    if (!this.created) {
      this.created = new Date();
    }
  }

}

