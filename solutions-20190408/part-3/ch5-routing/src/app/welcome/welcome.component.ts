import {Component, OnInit}       from '@angular/core';
import {AccountGeneratorService} from "../domain/account-generator.service";

@Component({
  selector:    'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls:   ['./welcome.component.css']
})
export class WelcomeComponent {
  imageUrl = 'https://picsum.photos/400/200?image=1060';
}
