import { Component, OnInit }     from '@angular/core';
import {AccountGeneratorService} from "../domain/account-generator.service";
import {Router}                  from "@angular/router";

@Component({
  selector: 'app-list-and-detail',
  templateUrl: './list-and-detail.component.html',
  styleUrls: ['./list-and-detail.component.css']
})
export class ListAndDetailComponent implements OnInit {

  constructor(
    private svc: AccountGeneratorService,
    private r: Router
  ) { }

  ngOnInit() {
  }

  get generator() {
    return this.svc;
  }

  show(id: number) {
    this.r.navigate(['/combined/detail', id]);
  }

}
