import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAndDetailComponent } from './list-and-detail.component';

describe('ListAndDetailComponent', () => {
  let component: ListAndDetailComponent;
  let fixture: ComponentFixture<ListAndDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAndDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAndDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
