import {BrowserModule} from '@angular/platform-browser';
import {NgModule}      from '@angular/core';

import {AppComponent}           from './app.component';
import {WelcomeComponent}       from './welcome/welcome.component';
import {ItemListComponent}      from './item-list/item-list.component';
import {ItemDetailComponent}    from './item-detail/item-detail.component';
import {NotFoundComponent}      from './not-found/not-found.component';
import {Route, RouterModule}    from "@angular/router";
import {ListAndDetailComponent} from './list-and-detail/list-and-detail.component';

export const routes: Route[] = [
  {path: 'welcome', component: WelcomeComponent},
  {path: 'list', component: ItemListComponent},
  {path: 'detail/:id', component: ItemDetailComponent},
  {
    path:     'combined', component: ListAndDetailComponent,
    children: [
      {path: 'detail/:id', component: ItemDetailComponent},
      {path: '', redirectTo: '/combined', pathMatch: 'full'},
    ]
  },
  {path: '', redirectTo: '/welcome', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    ItemListComponent,
    ItemDetailComponent,
    NotFoundComponent,
    ListAndDetailComponent
  ],
  imports:      [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers:    [],
  bootstrap:    [AppComponent]
})
export class AppModule {}
