import {Component, OnInit}       from '@angular/core';
import {AccountGeneratorService} from "../domain/account-generator.service";
import {Router}                  from "@angular/router";

@Component({
  selector:    'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls:   ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {

  constructor(
    private svc: AccountGeneratorService,
    private r: Router) {
  }

  ngOnInit() {
  }

  get generator() {
    return this.svc;
  }

  goto(id: number) {
    this.r.navigate(['/detail', id]);
  }

}
