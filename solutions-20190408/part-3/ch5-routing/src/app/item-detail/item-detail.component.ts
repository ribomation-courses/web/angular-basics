import {Component, OnInit}       from '@angular/core';
import {AccountGeneratorService} from "../domain/account-generator.service";
import {Observable, of}          from "rxjs";
import {Account}                 from "../domain/account";
import {ActivatedRoute}          from "@angular/router";
import {map, switchMap}          from "rxjs/operators";
import {tap}                     from "rxjs/internal/operators/tap";

@Component({
  selector:    'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls:   ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {
  account$: Observable<Account>;
  accountId: number;

  constructor(
    private svc: AccountGeneratorService,
    private r: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.account$ = this.r.params.pipe(
      map(p => p.id),
      tap(id => this.accountId = id),
      switchMap(id => of(this.svc.findById(id))),
    )
  }

}
