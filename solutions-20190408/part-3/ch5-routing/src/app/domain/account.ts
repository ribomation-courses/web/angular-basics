export class Account {
  public static rate: number = 0.25;

  constructor(
    private _id: number,
    private _accno: string,
    private _balance: number,
    private _credit: boolean,
    private _created: Date,
    private _owner: string
  ) {
  }

  get id(): number {
    return this._id;
  }

  get accno(): string {
    return this._accno;
  }

  get balance(): number {
    return this._balance;
  }

  get credit(): boolean {
    return this._credit;
  }

  get created(): Date {
    return this._created;
  }

  get owner(): string {
    return this._owner;
  }

  update(amount: number): void {
    const newBalance = this.balance + amount;
    if (newBalance >= 0 || this.credit) {
      this._balance = newBalance;
    }
  }

  toString(): string {
    return `Account[${this.accno}, SEK ${this.balance}, ${this.created.toDateString()}, ${this.owner}]`;
  }

}

