import {Injectable} from '@angular/core';
import {Account}    from "./account";

@Injectable({
  providedIn: 'root'
})
export class AccountGeneratorService {
  accounts: Account[] = [];

  constructor() {
    this.init(5);
  }

  public init(n: number) {
    this.accounts = [];
    for (let k = 0; k < n; ++k) {
      this.accounts.push(this.create());
    }
  }

  public findAll(): Account[] {
    return this.accounts;
  }

  public findById(id: number | string): Account {
    return this.accounts.find(acc => acc.id == +id);
  }

  public create(): Account {
    const id        = this.nextInt(1, 100000);
    const accno     = this.nextAccno();
    const balance   = this.nextInt(-10, +100);
    const hasCredit = (Math.random() <= 0.6);
    const date      = new Date(Date.now() - this.nextInt(5, 90) * 24 * 3600 * 1000);
    const customer  = this.nextElem(this.names);
    return new Account(id, accno, balance, hasCredit, date, customer);
  }

  private nextAccno() {
    return this.nextElem(this.banks) + this.nextInts(6, 1, 9);
  }

  private nextInt(lb: number, ub: number): number {
    if (ub <= lb) {
      throw new Error('invalid number range');
    }
    const interval = ub - lb + 1;
    const value    = Math.random() * interval + lb;
    return Math.floor(value);
  }

  private nextInts(n: number, lb: number, ub: number): string {
    let result = '';
    while (n-- > 0) {
      result += this.nextInt(lb, ub).toString(10);
    }
    return result;
  }

  private nextElem(arr: string[]): string {
    return arr[this.nextInt(0, arr.length - 1)];
  }

  private banks: string[] = ['seb', 'hb', 'sb', 'hsbc'];
  private names: string[] = [
    'Anna Conda', 'Ana Gram', 'Justin Time', 'Cris P. Bacon', 'Per Silja',
    'Inge Vidare', 'Sham Poo'
  ];
}
