import { interval } from 'rxjs';
import { map, filter, take, tap } from 'rxjs/operators';

interval(250)
    .pipe(
        tap(console.log),
        filter(n => (n + 1) % 4 === 0),
        map(_ => new Date()),
        map((d:Date) => d.toLocaleTimeString()),
        take(10)
    )
    .subscribe(value => console.info('#', value))
    ;
