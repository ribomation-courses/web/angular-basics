import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector:    'editable-text',
  templateUrl: './editable-text.component.html',
  styleUrls:   ['./editable-text.component.css']
})
export class EditableTextComponent  {
  @Input('value') value               = '';
  @Output('valueChange') valueEmitter = new EventEmitter<string>();
  @Output('save') saveEmitter         = new EventEmitter<string>();
  formVisible: boolean                = false;
  oldValue: string                    = undefined;

  showForm(): void {
    this.oldValue    = this.value;
    this.formVisible = true;
  }

  cancel(): void {
    this.formVisible = false;
    this.value       = this.oldValue;
    this.oldValue    = undefined;
  }

  save(): void {
    this.formVisible = false;
    this.oldValue    = undefined;
    this.saveEmitter.emit(this.value);
  }

  onKeyEvent(keyEvent: any): void {
    if (keyEvent.key === 'Enter') {
      this.save();
    } else if (keyEvent.key === 'Escape') {
      this.cancel();
    } else {
      //nothing
    }
  }

}
