import {Component, OnInit} from '@angular/core';
import {PersonsRepo}       from "./domain/persons-repo";
import {Person}            from "./domain/data.domain";

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent implements OnInit {
  persons: Person[];

  ngOnInit(): void {
    PersonsRepo.init();
    this.persons = PersonsRepo.findAll();
  }

  showById(id: number) {
    alert(`Person: ` + JSON.stringify(PersonsRepo.findById(id)));
  }

}
