import {Person, personsDB} from "./data.domain";

export class PersonsRepo {
  static persons;

 static init() {
    this.persons = personsDB.map(p => {
      p.male = (p.gender === "Male");
      return p;
    })
  }

  static findById(id: number): Person {
    return this.persons.find(p => p.id === id);
  }

  static findAll(): Person[] {
    return this.persons;
  }

}
