import {InjectionToken} from "@angular/core";

export const VAT_PERCENT = new InjectionToken<number>('VAT%');
export const SHIPPING_COST = new InjectionToken<number>('Shipping-Cost');
