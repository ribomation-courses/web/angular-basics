import {Account} from './account';

export class Generator {

    public static mk(): Account {
        const accno     = this.nextAccno();
        const balance   = this.nextInt(-10, +100);
        const hasCredit = (Math.random() <= 0.6);
        const date      = this.nextDate();
        const customer  = this.nextElem(this.names);

        return new Account(accno, balance, hasCredit, date, customer);
    }


    private static nextAccno() {
        return this.nextElem(this.banks).toUpperCase() + this.nextInts(6, 1, 9);
    }

    private static nextInt(lb: number, ub: number): number {
        if (ub <= lb) throw new Error('invalid number range');
        const interval = ub - lb + 1;
        const value    = Math.random() * interval + lb;
        return Math.floor(value);
    }

    private static nextInts(n: number, lb: number, ub: number): string {
        let result = '';
        while (n-- > 0) result += this.nextInt(lb, ub).toString(10);
        return result;
    }

    private static nextElem(arr: string[]): string {
        return arr[this.nextInt(0, arr.length - 1)];
    }

    private static nextDate():Date {
        return new Date(Date.now() - this.nextInt(5, 90) * Generator.DAY);
    }

    private static banks: string[] = ['seb', 'hb', 'sb', 'nb'];
    private static names: string[] = [
        'Anna Conda', 'Ana Gram', 'Justin Time', 'Cris P. Bacon', 'Per Silja',
        'Inge Vidare', 'Sham Poo'
    ];
    private static readonly DAY = 24 * 3600 * 1000;
}
