import {Component} from '@angular/core';
import {Account}   from './account';
import {Generator} from './generator';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent {
  accountList: Account[] = [];
  count: number          = 7;

  constructor() {
    this.generateAccounts(this.count);
  }

  generateAccounts(n: number) {
    console.info('[generateAccounts]', n);
    this.accountList = [];
    for (let k = 0; k < n; ++k) {
      this.accountList.push(Generator.create());
    }
  }


}
