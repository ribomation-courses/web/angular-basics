import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Video}      from '../domain/video.domain';

@Injectable({
    providedIn: 'root'
})
export class BackendService {
    private readonly baseUrl = 'http://localhost:3000/movies';

    constructor(private http: HttpClient) {
    }

    findAll(): Observable<Video[]> {
        return this.http.get<Video[]>(this.baseUrl);
    }

    findById(id: number | string): Observable<Video> {
        return this.http.get<Video>(`${this.baseUrl}/${id}`);
    }

}
