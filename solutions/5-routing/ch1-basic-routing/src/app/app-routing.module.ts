import {NgModule}             from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
    {
        path:         'home',
        loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)
    },
    {
        path:         'about',
        loadChildren: () => import('./pages/about/about.module').then(m => m.AboutModule)
    },
    {
        path:         'movies',
        loadChildren: () => import('./pages/movies/movies.module').then(m => m.MoviesModule)
    },
    {
        path:         'not-found',
        loadChildren: () => import('./pages/not-found/not-found.module').then(m => m.NotFoundModule)
    },
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: '**', loadChildren: () => import('./pages/not-found/not-found.module').then(m => m.NotFoundModule)}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
