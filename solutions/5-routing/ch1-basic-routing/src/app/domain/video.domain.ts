export interface Video {
    // "id": 1,
    // "title": "Helter Skelter",
    // "genre": "Crime|Drama|Thriller",
    // "link": "http://stumbleupon.com/sit/amet/nulla/quisque.js",
    // "image": "http://dummyimage.com/48x48.png/dddddd/000000"

    id: number;
    title: string;
    genre: string;
    link: string;
    image: string;
}
