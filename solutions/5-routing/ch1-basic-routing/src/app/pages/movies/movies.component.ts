import {Component, OnInit} from '@angular/core';
import {BackendService}    from '../../services/backend.service';
import {Observable}        from 'rxjs';
import {Video}             from '../../domain/video.domain';

@Component({
    selector:    'app-movies',
    templateUrl: './movies.component.html',
    styleUrls:   ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {
    videos$: Observable<Video[]>;

    constructor(private srv: BackendService) {
    }

    ngOnInit(): void {
        this.videos$ = this.srv.findAll();
    }

}
