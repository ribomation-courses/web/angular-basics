export interface Movie {
    id: number;
    title: string;
    genre: string;
    image: string;
    imageThumb: string;
    price: number;
}
