import {Component, OnInit} from '@angular/core';
import {ActivatedRoute}    from '@angular/router';
import {Observable}        from 'rxjs';
import {BackendService}    from '../../../services/backend.service';
import {Movie}             from '../../../domain/movie.domain';
import {map}               from 'rxjs/operators';

@Component({
    selector:    'app-movie-show',
    templateUrl: './movie-show.component.html',
    styleUrls:   ['./movie-show.component.scss']
})
export class MovieShowComponent implements OnInit {
    movie$: Observable<Movie>;

    constructor(
        private route: ActivatedRoute,
        private svc: BackendService
    ) {
    }

    ngOnInit(): void {
        const id = this.route.paramMap
            .pipe(map(p => p.get('id')))
            .subscribe(id => {
                this.movie$ = this.svc.findById(id);
            });
    }

    prevId(id: number): number {
        return Number(id) - 1;
    }

}
