import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MovieShowRoutingModule } from './movie-show-routing.module';
import { MovieShowComponent } from './movie-show.component';


@NgModule({
  declarations: [MovieShowComponent],
  imports: [
    CommonModule,
    MovieShowRoutingModule
  ]
})
export class MovieShowModule { }
