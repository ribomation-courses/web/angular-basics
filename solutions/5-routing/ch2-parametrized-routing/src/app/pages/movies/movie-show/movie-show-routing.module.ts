import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MovieShowComponent } from './movie-show.component';

const routes: Routes = [{ path: '', component: MovieShowComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovieShowRoutingModule { }
