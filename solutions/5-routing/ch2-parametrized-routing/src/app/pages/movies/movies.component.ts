import {Component, OnDestroy, OnInit} from '@angular/core';
import {BackendService}               from '../../services/backend.service';
import {Observable, Subscription}     from 'rxjs';
import {Movie}                        from '../../domain/movie.domain';

@Component({
    selector:    'app-movies',
    templateUrl: './movies.component.html',
    styleUrls:   ['./movies.component.scss']
})
export class MoviesComponent implements OnInit, OnDestroy {
    movies$: Observable<Movie[]>;
    firstPage: number  = 1;
    page: number       = this.firstPage;
    pageSize: number   = 5;
    lastPage: number   = 1;
    totalCount: number = this.pageSize;
    subs: Subscription;

    constructor(private srv: BackendService) {
    }

    ngOnInit(): void {
        this.movies$ = this.srv.findAllPaginated(this.page, this.pageSize);
        this.subs    = this.srv.totalCount().subscribe(n => {
            this.totalCount = n;
            this.lastPage   = this.totalCount / this.pageSize;
            console.debug('[movies] total=%d, last=%d', this.totalCount, this.lastPage);
        });
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    gotoFirstPage() {
        this.gotoPage(this.firstPage);
    }

    gotoPrevPage() {
        this.gotoPage(this.page - 1);
    }

    gotoNextPage() {
        this.gotoPage(this.page + 1);
    }

    gotoLastPage() {
        this.gotoPage(this.lastPage);
    }

    gotoPage(pageIdx: number) {
        this.page    = pageIdx;
        this.movies$ = this.srv.findAllPaginated(this.page, this.pageSize);
    }
}
