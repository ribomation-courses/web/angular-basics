import {NgModule}             from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {MoviesComponent} from './movies.component';

const routes: Routes = [
    {path: '', component: MoviesComponent},
    {path: 'info/:id', loadChildren: () => import('./movie-show/movie-show.module').then(m => m.MovieShowModule)}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MoviesRoutingModule {}
