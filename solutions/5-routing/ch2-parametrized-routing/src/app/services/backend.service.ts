import {Injectable}                                              from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable, of, throwError}                              from 'rxjs';
import {catchError, map}                                         from 'rxjs/operators';
import {Movie}                                                   from '../domain/movie.domain';

@Injectable({
    providedIn: 'root'
})
export class BackendService {
    private readonly baseUrl = 'http://localhost:3000/movies';

    constructor(private http: HttpClient) {
    }

    findAll(): Observable<Movie[]> {
        return this.http.get<Movie[]>(this.baseUrl);
    }

    findById(id: number): Observable<Movie> {
        return this.http.get<Movie>(`${this.baseUrl}/${id}`)
            .pipe(
                catchError((err: HttpErrorResponse) => {
                    if (err.status === 404) {
                        return of(BackendService.notFound(id));
                    } else {
                        throwError(err);
                    }
                })
            );
    }

    findAllPaginated(page: number, size: number = 5): Observable<Movie[]> {
        // @See https://github.com/typicode/json-server#paginate
        const q = new HttpParams().set('_page', page.toString()).set('_limit', size.toString());
        return this.http.get<Movie[]>(this.baseUrl, {params: q});
    }

    totalCount(): Observable<number> {
        const q = new HttpParams().set('_page', '1').set('_limit', '1');
        return this.http
            .get<number>(this.baseUrl, {params: q, observe: 'response'})
            .pipe(
                map((r: HttpResponse<number>) => {
                    // @See https://github.com/typicode/json-server#slice
                    return Number(r.headers.get('X-Total-Count').toString());
                })
            );
    }

    private static notFound(id: number): Movie {
        return {
            id:         id as number,
            title:      'not found',
            genre:      '--',
            price:      0,
            image:      '/assets/404.jpg',
            imageThumb: '/assets/404.jpg'
        };
    }

}
