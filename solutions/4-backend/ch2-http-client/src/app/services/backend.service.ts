import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User}       from '../domain/user';
import {map}        from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class BackendService {
    private readonly baseUrl: string = 'http://localhost:3000/users';
    private readonly prefix: string = '/assets/photos';

    constructor(private httpSvc: HttpClient) {
    }

    findAll(): Observable<User[]> {
        return this.httpSvc.get<User[]>(this.baseUrl)
            .pipe(
                map((users:User[]) => {
                    users.forEach(u => {
                        const id = Math.floor(Math.random() * 5) + 1;
                        u.photoUrl = `${this.prefix}/${id}.jpg`;
                    })
                    return users;
                })
            );
    }

    findById(id: number): Observable<User> {
        return this.httpSvc.get<User>(`${this.baseUrl}/${id}`);
    }

    delete(id: number): Promise<void> {
        return this.httpSvc
            .delete<any>(`${this.baseUrl}/${id}`)
            .toPromise();
    }

    create(user: User): Promise<User> {
        return this.httpSvc.post<User>(this.baseUrl, user).toPromise();
    }

    update(id: number, user: User): Promise<User> {
        return this.httpSvc
            .put<any>(`${this.baseUrl}/${id}`, user)
            .toPromise();
    }

}
