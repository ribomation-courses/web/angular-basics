import {Component, OnDestroy, OnInit} from '@angular/core';
import {BackendService}               from './services/backend.service';
import {User}                         from './domain/user';
import {Observable, Subscription}     from 'rxjs';

@Component({
    selector:    'app-root',
    templateUrl: './app.component.html',
    styleUrls:   ['./app.component.scss']
})
export class AppComponent implements OnInit {
    users$: Observable<User[]>;
    formUser: User;

    constructor(private backendSvc: BackendService) {
        this.clear();
    }

    ngOnInit(): void {
        this.users$ = this.backendSvc.findAll();
    }

    async delete(id: number) {
        await this.backendSvc.delete(id);
        this.users$ = this.backendSvc.findAll();
    }

    async save() {
        if (this.formUser.id === -1) {
            const usr = await this.backendSvc.create({
                name:  this.formUser.name,
                skill: this.formUser.skill,
                title: this.formUser.title,
            });
            console.debug('** created: %o', usr);
            this.clear();
            this.users$ = this.backendSvc.findAll();
        } else {
            const usr = await this.backendSvc.update(this.formUser.id, {
                name:  this.formUser.name,
                skill: this.formUser.skill,
                title: this.formUser.title,
            });
            console.debug('** updated: %o', usr);
            this.clear();
            this.users$ = this.backendSvc.findAll();
        }
    }

    edit(user: User) {
        this.formUser.id    = user.id;
        this.formUser.name  = user.name;
        this.formUser.title = user.title;
        this.formUser.skill = user.skill;
    }

    clear() {
        this.formUser = {id: -1, name: '', title: '', skill: ''};
    }

}
