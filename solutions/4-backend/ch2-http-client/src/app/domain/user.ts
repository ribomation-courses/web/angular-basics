export interface User {
    // "id": 1,
    // "name": "Estevan Marrill",
    // "skill": "LLBLGen",
    // "title": "Chemical Engineer"
    id?: number;
    name: string;
    skill: string;
    title: string;
    photoUrl?: string;
}
