import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

export interface Property {
    name: string;
    value: string;
}

function isEmpty(s: string) {
    return s === undefined
        || s === null
        || s.trim().length === 0;
}

@Component({
    selector:    'editable-text',
    templateUrl: './editable-text.widget.html',
    styleUrls:   ['./editable-text.widget.css']
})
export class EditableTextWidget implements OnInit {
    @Input('property') name: string;
    @Input('value') value: string;
    @Input('hint') hint: string   = 'Click to Edit';
    @Output('onSave') saveEmitter = new EventEmitter<Property>();
    formVisible: boolean          = false;
    formValue: string;

    constructor() {
    }

    ngOnInit() {
        if (isEmpty(this.name)) {
            throw new Error(`[editable-text] missing 'property'`);
        }
        if (isEmpty(this.value)) {
            throw new Error(`[editable-text] missing 'value'`);
        }
    }

    get empty(): boolean {
        return isEmpty(this.value);
    }

    edit() {
        this.formValue   = this.value;
        this.formVisible = true;
    }

    onKeyUp(ev: KeyboardEvent) {
        if (ev.code === 'Enter') {
            this.save();
        } else if (ev.code === 'Escape') {
            this.cancel();
        }
    }

    private save() {
        this.formVisible = false;
        this.value       = this.formValue;
        this.formValue   = undefined;
        this.saveEmitter.emit({
            name:  this.name,
            value: this.value
        });
    }

    private cancel() {
        this.formVisible = false;
        this.formValue   = undefined;
    }
}
