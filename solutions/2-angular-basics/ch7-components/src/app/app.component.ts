import {Component} from '@angular/core';
import {Property}  from './widgets/editable-text/editable-text.widget';

@Component({
    selector:    'app-root',
    templateUrl: './app.component.html',
    styleUrls:   ['./app.component.css']
})
export class AppComponent {
    name: string   = 'Per Silja';
    street: string = '17 Hacker Street';

    saved(payload: Property) {
        console.debug('[app] payload: %o', payload);
    }
}
