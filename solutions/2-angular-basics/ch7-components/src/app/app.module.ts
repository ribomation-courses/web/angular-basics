import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent }       from './app.component';
import { EditableTextWidget } from './widgets/editable-text/editable-text.widget';
import {FormsModule}          from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    EditableTextWidget,
  ],
    imports: [
        BrowserModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
