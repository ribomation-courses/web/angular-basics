import {Component} from '@angular/core';

export interface Item {
    name: string;
    price: number;
    count: number;
}

@Component({
    selector:    'app-root',
    templateUrl: './app.component.html',
    styleUrls:   ['./app.component.css']
})
export class AppComponent {
    items: Item[] = this.generate();

    generate(): Item[] {
        const mkPrice = () => Math.random() * 25 + 1;
        const mkCount = () => Math.random() * 10;
        const names   = ['apple', 'banana', 'coco nut', 'date plum'];
        return names.map(n => {
            return {name: n, price: mkPrice(), count: mkCount()} as Item;
        });
    }

    toggle() {
        if (this.items.length > 0) {
            this.items = [];
        } else {
            this.items = this.generate();
        }
    }

    get total(): number {
        return this.items
            .map(item => item.price * item.count)
            .reduce((sum, price) => sum + price, 0)
            ;
    }

}
