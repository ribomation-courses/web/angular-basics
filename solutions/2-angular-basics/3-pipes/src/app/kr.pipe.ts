import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'kr'
})
export class KrPipe implements PipeTransform {
  transform(value: number | string, oren: boolean = false): string {
    const amount = +value;
    
    const result = amount.toLocaleString('sv', {
      useGrouping: true,
      minimumFractionDigits: oren ? 2 : 0,
      maximumFractionDigits: oren ? 2 : 0
    }) + ' kr';

    return result.replace(/\s+/g, '\u00A0');
  }
}
