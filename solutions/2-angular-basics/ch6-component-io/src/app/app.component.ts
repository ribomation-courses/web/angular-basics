import {Component} from '@angular/core';

@Component({
    selector:    'app-root',
    templateUrl: './app.component.html',
    styleUrls:   ['./app.component.css']
})
export class AppComponent {
    fontSize: number = 2;
    sampleText: string = 'Tjolla Hopp';
    title1 = 'First use case';
    message: string;
}
