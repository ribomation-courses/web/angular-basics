import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule}    from '@angular/forms';
import { NumberStepperComponent } from './widgets/number-stepper/number-stepper.component';

@NgModule({
  declarations: [
    AppComponent,
    NumberStepperComponent
  ],
    imports: [
        BrowserModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
