import {MultiplyService} from './multiply.service';

export class MultBy3Service extends MultiplyService {
    get factor(): number {
        return 3;
    }

    compute(n: number): number {
        return this.factor * n;
    }

}
