import {Injectable} from '@angular/core';

@Injectable()
export abstract class MultiplyService {
    abstract compute(n: number): number;
    abstract get factor(): number ;
}
