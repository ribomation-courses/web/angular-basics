import {MultiplyService} from './multiply.service';

export class MultBy10Service extends MultiplyService {
    get factor(): number {
        return 10;
    }

    compute(n: number): number {
        return this.factor * n;
    }

}
