import {Component}          from '@angular/core';
import {MultiplyService}    from './services/multiply.service';
import {environment as env} from '../environments/environment.js';

@Component({
    selector:    'app-root',
    templateUrl: './app.component.html',
    styleUrls:   ['./app.component.css']
})
export class AppComponent {
    value: number = 15;
    PROD: boolean = env.production;

    constructor(private multSvc: MultiplyService) {
    }

    get mult(): MultiplyService {
        return this.multSvc;
    }
}
