import {BrowserModule} from '@angular/platform-browser';
import {NgModule}      from '@angular/core';

import {AppComponent}    from './app.component';
import {FormsModule}     from '@angular/forms';
import {MultiplyService} from './services/multiply.service';
import {MultBy3Service}  from './services/multBy3.service';
import {MultBy10Service} from './services/multBy10.service';
import {environment}     from '../environments/environment.js';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports:      [
        BrowserModule,
        FormsModule
    ],
    providers:    [
        {
            provide:    MultiplyService,
            useFactory: () => environment.service !== 10 ? new MultBy3Service() : new MultBy10Service()
        }
    ],
    bootstrap:    [AppComponent]
})
export class AppModule {}
