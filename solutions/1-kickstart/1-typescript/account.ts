export class Account {
    constructor(
        private _accno: string,
        private _balance: number,
        private _credit: boolean,
        private _created: Date,
        private _owner: string
    ) {
    }

    get accno(): string {
        return this._accno.toUpperCase();
    }

    get balance(): number {
        return this._balance;
    }

    get credit(): boolean {
        return this._credit;
    }

    set credit(b:boolean) {this._credit=b;}

    get created(): Date {
        return this._created;
    }

    get owner(): string {
        return this._owner;
    }

    update(amount: number): void {
        const newBalance = this.balance + amount;
        if (newBalance >= 0 || this.credit) this._balance = newBalance;
    }

    toString():string {
        return `Account[${this.accno}, SEK ${this.balance}, ${this.created.toDateString()}, ${this.owner}]`;
    }

}

