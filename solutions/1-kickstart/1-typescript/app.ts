import {Account}   from "./account";
import {Generator} from "./generator";

const N = Number(process.argv[2]) || 10;
const g = new Generator();

let accounts: Account[] = [];
for (let k = 0; k < N; ++k) 
    accounts.push(g.create());

for (let k = 0; k < accounts.length; ++k)
    console.log(accounts[k].toString());

