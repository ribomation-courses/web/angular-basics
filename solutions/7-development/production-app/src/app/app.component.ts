import {Component}       from '@angular/core';
import {ProductsService} from './services/products.service';

@Component({
    selector:    'app-root',
    templateUrl: './app.component.html',
    styleUrls:   ['./app.component.scss']
})
export class AppComponent {
    currentYear: number = new Date().getFullYear();

    constructor(private productsSvc: ProductsService) {
    }

    updateSimulate(ev: any) {
        console.log('updateSimulate', ev.target.checked);
        this.productsSvc.empty = ev.target.checked;
    }
}
