import {Component, OnInit} from '@angular/core';

@Component({
    selector:  'app-not-found',
    template:  `
                   <h1>Page Not Found</h1>
                   <p>Please, go to a real page !!</p>
               `,
    styles: []
})
export class NotFoundComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

}
