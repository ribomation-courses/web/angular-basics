import {Component, OnInit} from '@angular/core';
import {ProductsService}   from '../../services/products.service';
import {ActivatedRoute}    from '@angular/router';
import {Observable}        from 'rxjs';
import {Product}           from '../../domain/product';

@Component({
    selector:    'app-product-view',
    templateUrl: './product-view.component.html',
    styleUrls:   ['./product-view.component.scss']
})
export class ProductViewComponent implements OnInit {
  item$: Observable<Product>;

    constructor(
        private productsSvc: ProductsService,
        private currentRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
        const productId = this.currentRoute.snapshot.paramMap.get('productId');
        //console.log('productId', productId);
        this.item$ = this.productsSvc.findById(+productId);
    }

}
