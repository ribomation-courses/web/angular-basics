import {Component, OnInit} from '@angular/core';
import {formatDate}        from '@angular/common';

@Component({
    selector:    'app-root',
    templateUrl: './app.component.html',
    styleUrls:   ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title             = 'date / range';
    date: Date        = new Date();
    offset: number    = 10;
    negative: boolean = false;

    ngOnInit() {
    }

    fmtDate(date: Date) {
        return formatDate(date, 'yyyy-MM-dd', 'en');
    }

    nextDate(date: Date, numDays: number): Date {
        const DAY_MS = 24 * 3600 * 1000;
        const offset = (this.negative ? -1 : +1) * numDays * DAY_MS;
        return new Date(date.getTime() + offset);
    }

    onDateChanged(dateTxt: string) {
        const date = new Date(dateTxt);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
        this.date = date;
    }

}
