import {Component} from '@angular/core';
import {User}      from './domain/user';
import {fn}        from '@angular/compiler/src/output/output_ast';

@Component({
    selector:    'app-root',
    templateUrl: './app.component.html',
    styleUrls:   ['./app.component.scss']
})
export class AppComponent {
    user: User                   = this.mkUser();
    showTemplateForm: boolean    = false;
    showComponentForm: boolean   = false;
    showInteractiveForm: boolean = true;

    onSubmitForm(payload: any) {
        console.log('form payload', payload);
        alert(`Form Data: ${JSON.stringify(payload)}`);
    }

    onSelected(item: User) {
        console.log('Selected user', item);
        alert(`Selected User: ${JSON.stringify(item)}`);
    }

    users: User[] = Array.from({length: 50}).map(this.mkUser);

    private mkUser(): User {
        const fnames = ['anna', 'bertil', 'carin', 'david', 'elin', 'filip', 'greta',
            'hans', 'ida', 'jonas', 'kajsa', 'lars', 'monika', 'nils'];
        const lnames = ['anderson', 'bertilson', 'kvist', 'skogh', 'petterson', 'svensson', 'jonsson'];

        const pick = (arr: string[]) => arr[Math.floor(Math.random() * arr.length)];
        const age  = () => Math.floor(20 + 60 * Math.random());
        const tc   = (s: string) => s.substr(0, 1).toUpperCase() + s.substr(1);
        const name = tc(pick(fnames)) + ' ' + tc(pick(lnames));

        return {name: name, age: age()};
    }

}
