import {BrowserModule} from '@angular/platform-browser';
import {NgModule}      from '@angular/core';

import {AppComponent}                  from './app.component';
import {TemplateOrientedFormComponent}    from './panes/template-oriented-form/template-oriented-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ComponentOrientedFormComponent } from './panes/component-oriented-form/component-oriented-form.component';
import { InteractiveFormComponent } from './panes/interactive-form/interactive-form.component';

@NgModule({
    declarations: [
        AppComponent,
        TemplateOrientedFormComponent,
        ComponentOrientedFormComponent,
        InteractiveFormComponent
    ],
    imports:      [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers:    [],
    bootstrap:    [AppComponent]
})
export class AppModule {}
