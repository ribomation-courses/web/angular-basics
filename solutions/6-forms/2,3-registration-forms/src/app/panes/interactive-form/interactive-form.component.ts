import {
    Component,
    OnInit,
    EventEmitter,
    Input,
    Output
}                       from '@angular/core';
import {FormControl}    from '@angular/forms';
import {Observable, of} from 'rxjs';
import {
    debounceTime,
    distinctUntilChanged, filter,
    map,
    switchMap
}                       from 'rxjs/operators';
import {User}           from '../../domain/user';

@Component({
    selector:    'interactive-form',
    templateUrl: './interactive-form.component.html',
    styleUrls:   ['./interactive-form.component.scss']
})
export class InteractiveFormComponent implements OnInit {
    @Input('searchable') searchable: User[];
    @Input('hint') hint: string         = 'Search...';
    @Output('selected') selectedEmitter = new EventEmitter<any>();
    results$: Observable<User[]>;

    phrase: FormControl = new FormControl();

    ngOnInit() {
        if (!this.searchable || this.searchable.length === 0) {
            throw new Error('empty searchable');
        }

        console.debug('** searchable: %o', this.searchable);

        this.results$ = this.phrase.valueChanges
            .pipe(
                debounceTime(200),
                distinctUntilChanged(),
                filter((txt: string) => txt.length === 0 || txt.length >= 2),
                switchMap(txt => this.fakeSearch(txt))
            );
    }

    onClicked(item: any) {
        this.selectedEmitter.emit(item);
    }

    private fakeSearch(phrase: string): Observable<User[]> {
        phrase       = phrase.replace(/[^a-z0-9åäöÅÄÖ*]/ig, '');
        if (!phrase || phrase.trim().length === 0) {
            return of([]);
        }
        if (phrase === '***') {
            return of(this.searchable);
        }

        const result = this.searchable
            .map(obj => Object.assign({}, obj)) //clone
            .filter(item => {
                if (item.age.toString() == phrase) {
                    // @ts-ignore
                    item.age = `<span class="highlight">${item.age}</span>`;
                    return true;
                }
                const re = new RegExp('^(.*)(' + phrase + ')(.*)$', 'i');
                const m  = re.exec(item.name);
                if (m) {
                    item.name = m[1] + '<span class="highlight">' + m[2] + '</span>' + m[3];
                    return true;
                }
                return false;
            });

        return of(result);
    }

}
