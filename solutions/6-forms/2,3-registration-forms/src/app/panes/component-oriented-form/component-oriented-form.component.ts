import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User}                                           from '../../domain/user';
import {FormControl, FormGroup, Validators}             from '@angular/forms';

@Component({
    selector:    'component-oriented-form',
    templateUrl: './component-oriented-form.component.html',
    styleUrls:   ['./component-oriented-form.component.scss']
})
export class ComponentOrientedFormComponent implements OnInit {
    @Input('user') user: User;
    @Output('submit') submitEmitter = new EventEmitter<User>();

    form: FormGroup;
    name: FormControl;
    age: FormControl;

    constructor() {
    }

    ngOnInit() {
        if (!this.user) {
            throw new Error('no user object provided');
        }
        this.name = new FormControl(this.user.name, [
            Validators.required, Validators.minLength(5)
        ]);
        this.age  = new FormControl(this.user.age, [
            Validators.min(20), Validators.max(80)
        ]);
        this.form = new FormGroup({
            name: this.name,
            age:  this.age
        });
    }

    onSubmit() {
        if (this.form.valid) {
            this.submitEmitter.emit(this.form.value);
        }
    }
}
