import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DemoNgifComponent } from './demo-ngif.component';
import { DemoNgif2Component } from './demo-ngif-2.component';
import { DemoNgswitchComponent } from './demo-ngswitch.component';
import { DemoNgforComponent } from './demo-ngfor.component';
import { DemoNgfor2Component } from './demo-ngfor-2.component';
import { DemoNgclassComponent } from './demo-ngclass.component';
import { DemoNgstyleComponent } from './demo-ngstyle.component';
import { ExerciseDirectivesComponent } from './exercise-directives.component';
import {FormsModule} from "@angular/forms";
import { DemoJsonComponent } from './demo-json.component';
import { DemoAsyncComponent } from './demo-async.component';
import { DemoTextTransformationsComponent } from './demo-text-transformations.component';
import { DemoNumberFormatsComponent } from './demo-number-formats.component';
import { DemoSublistComponent } from './demo-sublist.component';
import { DemoDateFormatsComponent } from './demo-date-formats.component';
import { TrainCasePipe } from './train-case.pipe';
import { DemoTraincaseComponent } from './demo-traincase.component';
import { NumberStepperComponent } from './number-stepper.component';
import { DemoNumberStepperComponent } from './demo-number-stepper.component';

@NgModule({
  declarations: [
    AppComponent,
    DemoNgifComponent,
    DemoNgif2Component,
    DemoNgswitchComponent,
    DemoNgforComponent,
    DemoNgfor2Component,
    DemoNgclassComponent,
    DemoNgstyleComponent,
    ExerciseDirectivesComponent,
    DemoJsonComponent,
    DemoAsyncComponent,
    DemoTextTransformationsComponent,
    DemoNumberFormatsComponent,
    DemoSublistComponent,
    DemoDateFormatsComponent,
    TrainCasePipe,
    DemoTraincaseComponent,
    NumberStepperComponent,
    DemoNumberStepperComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
