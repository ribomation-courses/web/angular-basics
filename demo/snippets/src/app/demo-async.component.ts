import {Component, OnInit} from "@angular/core";
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/interval";
import "rxjs/add/operator/map";

@Component({
  selector: "app-demo-async",
  template: `
    <p>
      Time is {{(currentTime$ | async) | date:'mediumTime'}}
    </p>
    <p>
      Date&amp;Time is {{(currentTime$ | async) | date:'yyyy-MM-dd HH:mm:ss'}}
    </p>
  `,
  styles: []
})
export class DemoAsyncComponent implements OnInit {
  currentTime$: Observable<Date>;
  ngOnInit() {
    this.currentTime$ = Observable.interval(1000)
      .map(() => new Date())
    ;
  }
}

