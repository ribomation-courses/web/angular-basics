import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {FormControl} from "@angular/forms";
import {OperandsService} from "../operands.service";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import 'rxjs/add/operator/filter';

@Component({
  selector: "app-operands",
  templateUrl: "./operands.component.html",
  styleUrls: ["./operands.component.css"]
})
export class OperandsComponent implements OnInit {
  @Input("hideRight") hideRight: boolean = false;
   lhsField: FormControl;
   rhsField: FormControl;

  constructor(private opsSvc: OperandsService) {
    this.lhsField = new FormControl();
    this.rhsField = new FormControl();
  }

  ngOnInit() {
    this.opsSvc.setSources(this.lhsField, this.rhsField);
    setTimeout(() => {
      this.lhsField.setValue(40);
      this.rhsField.setValue(2);
    }, 1000);
  }

  compute(): void {
    this.lhsField.setValue(this.lhsField.value);
    this.rhsField.setValue(this.rhsField.value);
  }

}
