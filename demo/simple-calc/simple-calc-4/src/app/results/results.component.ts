import {Component, OnInit} from "@angular/core";
import {Result} from "../result";
import {HttpEvalService} from "../eval/http-eval.service";
import {OperandsService} from "../operands.service";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/do";

@Component({
  selector: "app-results",
  templateUrl: "./results.component.html",
  styleUrls: ["./results.component.css"]
})
export class ResultsComponent implements OnInit {
  results: Observable<Result[]>;
  loading: boolean = false;

  constructor(private httpEvalSvc: HttpEvalService,
              private opsSvc: OperandsService) {
  }

  ngOnInit() {
    this.results = this.opsSvc.feed()
      .do(() => {
        this.loading = true;
      })
      .switchMap(v => this.httpEvalSvc.computeFeed("all", v.lhs, v.rhs))
      .do(() => {
        this.loading = false;
      })
    ;
  }

}
