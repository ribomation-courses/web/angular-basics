import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";

import {AppComponent} from "./app.component";
import {AppRoutingModule} from "./app-routing.module";
import {NotFoundComponent} from "./not-found/not-found.component";
import {OperandsComponent} from "./operands/operands.component";
import {ResultsComponent} from "./results/results.component";
import {ResultsAdvancedComponent} from "./results-advanced/results-advanced.component";
import {EvalService} from "./eval/eval.service";
import {HttpEvalService} from "./eval/http-eval.service";
import {OperandsService} from "./operands.service";

@NgModule({
  declarations: [
    AppComponent,
    OperandsComponent,
    ResultsComponent,
    ResultsAdvancedComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [EvalService, HttpEvalService, OperandsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
