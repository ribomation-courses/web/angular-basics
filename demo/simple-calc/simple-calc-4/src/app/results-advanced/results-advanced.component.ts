import { Component, OnInit } from '@angular/core';
import {Result} from "../result";
import {OperandsService} from "../operands.service";
import {HttpEvalService} from "../eval/http-eval.service";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/do";

@Component({
  selector: 'app-results-advanced',
  templateUrl: './results-advanced.component.html',
  styleUrls: ['./results-advanced.component.css']
})
export class ResultsAdvancedComponent implements OnInit {
  results: Observable<Result[]>;
  loading: boolean = false;

  constructor(private httpEvalSvc: HttpEvalService,
              private opsSvc: OperandsService) {
  }

  ngOnInit() {
    this.results = this.opsSvc.feed()
      .do(() => {
        this.loading = true;
      })
      .switchMap(v => this.httpEvalSvc.computeFeed("adv", v.lhs))
      .do(() => {
        this.loading = false;
      })
    ;
  }

}
