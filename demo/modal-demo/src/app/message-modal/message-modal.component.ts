import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'message-modal',
  templateUrl: './message-modal.component.html',
  styleUrls: ['./message-modal.component.scss']
})
export class MessageModalComponent {
  @Input('open') isOpen: boolean = false;
  @Output('openChange') openEmitter = new EventEmitter<boolean>();
  @Input('title') title: string = 'Message';

  close(modal: any) {
    modal.classList.remove('active');
    this.openEmitter.emit(false);
  }
}
