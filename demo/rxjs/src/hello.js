"use strict";
exports.__esModule = true;
var Rx = require("rxjs/Rx");
Rx.Observable.range(1, 20)
    .filter(function (n) { return n % 3 !== 0; })
    .map(function (x) { return x * x; })
    .subscribe(function (n) { return console.log(n); });
