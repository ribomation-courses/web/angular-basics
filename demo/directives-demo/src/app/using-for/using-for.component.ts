import { Component, OnInit } from '@angular/core';

interface Person {
  name: string;
  age: number;
  female: boolean;
}

function randInt(): number {
  return Math.floor(15 + Math.random() * 65);
}
function randBool(): boolean {
  return Math.random() < 0.5;
}
function randTxt(): string {
  const names = [
    'Anna Conda', 'Anna Gram', 'Justin Time', 'Sham Poo',
    'Per Silja', 'Inge Vidare'
  ];
  const idx = Math.floor(names.length * Math.random());
  return names[idx];
}

function mkPerson(): Person {
  return {
    name: randTxt(), age: randInt(), female: randBool()
  };
}

@Component({
  selector: 'app-using-for',
  templateUrl: './using-for.component.html',
  styleUrls: ['./using-for.component.css']
})
export class UsingForComponent implements OnInit {
  persons: Person[] = [];
  count: number     = 5;
  user: Person      = mkPerson();

  ngOnInit() { this.onChange(); }
  onChange() {
    this.persons = [];
    for (let k = 0; k < this.count; ++k) {
      this.persons.push( mkPerson() );
    }
  }
}


