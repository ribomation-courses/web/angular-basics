import { Component } from '@angular/core';

@Component({
  selector: 'app-css-class',
  templateUrl: './css-class.component.html',
  styleUrls: ['./css-class.component.css']
})
export class CssClassComponent {
  cssName = 'light';
  value   = 1;

  toggleCssClass() {
    this.cssName = (this.cssName === 'light') ? 'dark' : 'light';
  }

  incrementValue() {
    ++this.value;
  }
}


