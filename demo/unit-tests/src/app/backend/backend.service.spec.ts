import {TestBed, inject} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {BackendService} from './backend.service';

export interface Person {
  id?: number;
  name: string;
  age: number;
}

describe('BackendService', () => {
  let http: HttpClient;
  let httpCtrl: HttpTestingController;
  const baseUri = 'http://localhost:4242/api';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BackendService]
    });

    http = TestBed.get(HttpClient);
    httpCtrl = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpCtrl.verify();
  });

  it('should be created', inject([BackendService], (service: BackendService) => {
    expect(service).toBeDefined();
  }));

  it('can perform a (fake) http get request', () => {
    const fakePerson: Person = {
      id: 17, name: 'Inge Vidare', age: 42
    };
    const url = `${baseUri}/persons/${fakePerson.id}`;

    http.get<Person>(url)
      .subscribe(data => {
        expect(data).toBeDefined();
        expect(data.name).toBe('Inge Vidare');
        expect(data.age).toBe(42);
      });

    const req = httpCtrl.expectOne(url);
    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');

    req.flush(fakePerson); //trigger the (fake) call
  });

  it('can perform a (fake) http post request', () => {
    const input: Person = {
      name: 'Justin Time', age: 42
    };
    const expected: Person = {
      id: 17, name: 'Justin Time', age: 42
    };
    const url = `${baseUri}/persons`;

    http.post<Person>(url, input)
      .subscribe(data => {
        expect(data).toBeDefined();
        expect(data.id).toBe(expected.id);
        expect(data.name).toBe(expected.name);
        expect(data.age).toBe(expected.age);
      });

    const req = httpCtrl.expectOne(url);
    expect(req.request.method).toBe('POST');

    req.flush(expected);
  });

});
