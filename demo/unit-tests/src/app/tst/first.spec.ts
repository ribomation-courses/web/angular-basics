describe('1st test',  () => {
  beforeEach(() => {
    console.log('runs before a test');
  });

  afterEach(() => {
    console.log('runs after a test');
  });

  it('should deliver the answer to everything', function () {
    expect(40 + 2).toBe(42);
  });
});
