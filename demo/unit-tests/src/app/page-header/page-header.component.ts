import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-page-header',
  template: `
    <div class="row">
      <div *ngIf="!loggedIn">Please, log in!</div>
      <div *ngIf="loggedIn" >Welcome {{username}}</div>
    </div>
  `,
  styles: [`div.row {border: solid 1px gray;}`]
})
export class PageHeaderComponent implements OnInit {
  loggedIn: boolean = false;
  username: string;

  constructor(private auth: AuthService) {}

  ngOnInit() {
    this.loggedIn = this.auth.isAuthenticated();
    if (this.loggedIn) {
      this.username = this.auth.getUsername();
    }
  }
}


