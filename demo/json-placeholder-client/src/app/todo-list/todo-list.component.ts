import { Component, OnInit } from '@angular/core';
import {JsonPlaceholderService} from '../json-placeholder/json-placeholder.service';
import {Observable} from 'rxjs/Observable';
import {Todo} from '../model/todo.model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  todos: Observable<Todo[]>;

  constructor(private todoSvc: JsonPlaceholderService) {
  }

  ngOnInit(): void {
    this.todos = this.todoSvc.list();
  }

}
