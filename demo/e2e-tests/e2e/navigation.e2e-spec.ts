import {HomePage} from './pages/home.page';
import {NavbarPage} from './pages/navbar.page';
import {browser} from 'protractor';

describe('Page: Navbar', () => {
  let page: NavbarPage;

  beforeEach(() => {
    page = new NavbarPage();
    page.navigateTo();
  });

  it('should start at Home', () => {
    expect(browser.getCurrentUrl()).toContain('/home');
  });

  it('should navigate to about, when clicking on About', () => {
    page.getAboutLink().click();
    expect(browser.getCurrentUrl()).toContain('/about');
  });

  it('should navigate to products, when clicking on Products', () => {
    page.getProductsLink().click();
    expect(browser.getCurrentUrl()).toContain('/products');
  });

});
