import { browser, by, element } from 'protractor';

export class ProductsPage {
  navigateTo() {
    return browser.get('/products');
  }

  getHeaderText() {
    return element(by.css('app-products h1')).getText();
  }

  getProductTableRows() {
    return element.all(by.css('app-products tbody tr'));
  }


}
