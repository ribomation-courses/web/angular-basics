import { browser, by, element } from 'protractor';

export class AboutPage {
  navigateTo() {
    return browser.get('/about');
  }

  getHeaderText() {
    return element(by.css('app-about h1')).getText();
  }

  getNameText() {
    return element(by.css('app-about tbody tr:nth-child(1) td')).getText();
  }


}
