function sum(n) {
    if (n < 0) return NaN;
    if (n === 0) return 0;
    if (n === 1) return 1;
    return n + sum(n - 1);
}

function prod(n) {
    if (n < 0) return NaN;
    if (n === 0) return 0;
    if (n === 1) return 1;
    return n * prod(n - 1);
}

function fib(n) {
    if (n < 0) return NaN;
    if (n === 0) return 0;
    if (n === 1) return 1;
    return fib(n - 2) + fib(n - 1);
}

function delayedValue(callback) {
    setTimeout(() => {
        callback(42);
    }, 1000);
}

function promisedValue() {
    return new Promise((ok) => {
        setTimeout(() => {
            ok(42);
        }, 1000);
    }, (err) => {
        err(-1);
    });
}

module.exports = {
    sum: sum,
    prod: prod,
    fib: fib,
    delayedValue: delayedValue,
    promisedValue: promisedValue,
};

