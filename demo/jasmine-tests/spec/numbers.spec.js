const numbers = require('../numbers');

describe('base cases', () => {
    it('should return NaN for negative arguments', () => {
        const value = -1;
        expect(numbers.sum(value)).toBeNaN();
        expect(numbers.prod(value)).toBeNaN();
        expect(numbers.fib(value)).toBeNaN();
    });

    it('should return 0 for argument 0', () => {
        const value = 0;
        expect(numbers.sum(value)).toBe(value);
        expect(numbers.prod(value)).toBe(value);
        expect(numbers.fib(value)).toBe(value);
    });

    it('should return 1 for argument 1', () => {
        const value = 1;
        expect(numbers.sum(value)).toBe(value);
        expect(numbers.prod(value)).toBe(value);
        expect(numbers.fib(value)).toBe(value);
    });
});

describe('simple cases', () => {
    it('sum(10) should be 55', () => {
        expect(numbers.sum(10)).toBe(55);
    });
    it('prod(5) should be 120', () => {
        expect(numbers.prod(5)).toBe(120);
    });
    it('fib(10) should be 55', () => {
        expect(numbers.fib(10)).toBe(55);
    });
});

describe('callback tests', () => {
    let value = 0;
    beforeEach((done) => {
        numbers.delayedValue((n) => {
            value = n;
            done();
        });
    });
    it('should get 42 after 1 sec', (done) => {
        expect(value).toBe(42);
        done();
    });
});

describe('promise tests', () => {
    it('should get 42 after 1 sec', (done) => {
        numbers.promisedValue()
            .then((n) => {
                expect(n).toBe(42);
                done();
            })
        ;
    });
});



