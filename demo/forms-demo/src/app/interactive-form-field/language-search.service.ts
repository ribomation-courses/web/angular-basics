import {Injectable} from '@angular/core';
import {of}         from 'rxjs';
import {delay}      from 'rxjs/operators';
import {Observable} from 'rxjs';

const languagesDB = [
  'Ada', 'APL',
  'B', 'BASIC',
  'C', 'C++', 'C#', 'COBOL',
  'Dart',
  'Erlang', 'Eiffel',
  'Fortran', 'Forth',
  'Go',
  'Haskel',
  'Icon',
  'Java', 'JavaScript',
  'Kotlin',
  'Lua', 'LISP',
  'ML', 'Modula',
  'NQC',
  'Objective-C', 'Occam',
  'Pascal', 'Python', 'Plex', 'Prolog', 'Perl',
  'Ruby', 'Rust',
  'Simula', 'SmallTalk',
  'TypeScript', 'TeX',
  'VisualBasic', 'VHDL',
];

@Injectable({providedIn: 'root'})
export class LanguageSearchService {
  search(phrase: string): Observable<string[]> {
    if (!phrase || phrase.trim().length === 0) {phrase = ' ';}

    const PHRASE = phrase.toLowerCase();
    const result = languagesDB
      .filter(lang => lang.toLowerCase().includes(PHRASE))
      .sort((lhs, rhs) => {
        const LHS = lhs.toLowerCase();
        const RHS = rhs.toLowerCase();
        if (LHS === RHS) {return 0;}
        return LHS < RHS ? -1 : +1;
      });

    return of(result)
      .pipe(delay(400)); //just, for the fun of it ;-)
  }
}


