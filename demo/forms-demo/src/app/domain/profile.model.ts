export class Profile {
  name: string;
  age: number;
  email: string;
  email2: string;

  reset() {
    this.name   = undefined;
    this.age    = undefined;
    this.email  = undefined;
    this.email2 = undefined;
  }
}
