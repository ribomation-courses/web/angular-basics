import {Component, ViewChild} from '@angular/core';
import {Profile}              from '../domain/profile.model';
import {FormGroup}            from '@angular/forms';

@Component({
  selector:    'app-template-oriented',
  templateUrl: './template-oriented.component.html',
  styleUrls:   ['../app.component.css']
})
export class TemplateOrientedComponent {
  profile: Profile = new Profile();
  @ViewChild('f') form: FormGroup;

  onSubmit() {
    alert('Profile submitted: ' + JSON.stringify(this.profile));
    this.profile.reset();
    this.form.reset();
  }
}
