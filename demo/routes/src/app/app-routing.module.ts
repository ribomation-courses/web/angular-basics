import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {FirstComponent} from "./first.component";
import {SecondComponent} from "./second.component";
import {ThirdComponent} from "./third.component";
import {FakeDetailsComponent} from "./fake-details.component";
import {FakeDataComponent} from "./fake-data.component";
import {ProductSpecificationComponent} from "./products/product-specification.component";
import {ProductOverviewComponent} from "./products/product-overview.component";
import {ProductsComponent} from "./products/products.component";
import {AuthService} from "./auth.service";
import {AuthRequiredGuard} from "./auth-required.guard";
import {LoginComponent} from "./login.component";
import {AdminComponent} from "./admin.component";

const routes: Routes = [
  {path: "first", component: FirstComponent},
  {path: "second", component: SecondComponent},
  {path: "third", component: ThirdComponent},
  {path: "detail/:name", component: FakeDetailsComponent},
  {path: "fake", component: FakeDataComponent, data: {title: "Fake Data Viewer"}},
  {
    path: "products", component: ProductsComponent,
    children: [
      {path: "overview", component: ProductOverviewComponent},
      {path: "spec", component: ProductSpecificationComponent},
      {path: "", redirectTo: "overview", pathMatch: "full"},
    ]
  },
  {path: "login", component: LoginComponent},
  {path: "admin", component: AdminComponent, canActivate: [AuthRequiredGuard]},
  {path: "", redirectTo: "first", pathMatch: "full"},
  {path: "**", redirectTo: "first"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthRequiredGuard]
})
export class AppRoutingModule {
}
