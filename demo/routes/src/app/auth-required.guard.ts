import {Injectable} from "@angular/core";
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {AuthService} from "./auth.service";

@Injectable()
export class AuthRequiredGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<boolean> | Promise<boolean> | boolean
  {
    if (this.auth.isAuthenticated()) {
      console.log("[auth]", "canActivate", "granted", next.toString());
      return true;
    }

    console.log("[auth]", "canActivate", "denied", next.toString());
    this.router.navigate(["/login"]);
    return false;
  }
}
