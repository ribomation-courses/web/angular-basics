function drawLotteryTicket(numSecs) {
    console.log('drawing the lottery...');
    return new Promise((ok, err) => {
        setTimeout(() => {
            let number = Math.random();
            if (number < 0.75) {
                ok(`++ winner ${number}`);
            } else {
                err('-- looser');
            }
        }, numSecs * 1000);
    });
}

let first  = drawLotteryTicket(1);
let second = drawLotteryTicket(1);
let third  = drawLotteryTicket(1);

Promise.all([first, second, third])
    .then(ok => {
        console.info('all winners', ok);
    }, err => {
        console.error('somebody lost ', err);
    });

