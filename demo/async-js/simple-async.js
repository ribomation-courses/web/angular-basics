function drawLotteryTicket(numSecs) {
    console.log('drawing the lottery...');
    return new Promise((ok, err) => {
        setTimeout(() => {
            let number = Math.random();
            if (number < 0.75) {
                ok(`++ winner ${number}`);
            } else {
                err('-- looser');
            }
        }, numSecs * 1000);
    });
}

async function usePromise() {
    try {
        let first = await drawLotteryTicket(1);
        console.info(first);
        let second = await drawLotteryTicket(1);
        console.info(second);
        let third = await drawLotteryTicket(1);
        console.info(third);
    } catch (err) {
        console.error('short-cutting', err);
    }
}

usePromise();


