function drawLotteryTicket(numSecs) {
    console.log('drawing the lottery...');
    return new Promise((ok, err) => {
        setTimeout(() => {
            let number = Math.random();
            if (number < 0.75) {
                ok(`++ winner ${number}`);
            } else {
                err('-- looser');
            }
        }, numSecs * 1000);
    });
}

function usePromise() {
    drawLotteryTicket(1)
        .then((result) => {
            console.info(result);
            return drawLotteryTicket(1);
        }, (err) => {
            console.error(err);
            return drawLotteryTicket(1);
        })
        .then((result) => {
            console.info(result);
            return drawLotteryTicket(1);
        }, (err) => {
            console.error(err);
            return drawLotteryTicket(1);
        })
        .then((result) => {
            console.info(result);
        }, (err) => {
            console.error(err);
        })
    ;
}

usePromise();
