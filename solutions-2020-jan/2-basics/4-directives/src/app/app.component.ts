import { Component } from '@angular/core';
import { Product, ProductGenerator } from './product.domain';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'shopping cart';
  cart: Product[] = [];
  count: number = 4;

  constructor() {
    this.onChange();
  }

  onChange() {
    const gen = new ProductGenerator();
    this.cart = gen.generate(this.count);
  }

  get sum():number {
    return this.cart
    .map(p => p.total)
    .reduce((sum, t) => sum + t , 0)
    ;
  }
}
