import { Component, Input, Output, EventEmitter } from '@angular/core';

type PropertyType = string|number|boolean;

@Component({
  selector: 'editable-text',
  templateUrl: './editable-text.component.html',
  styleUrls: ['./editable-text.component.scss']
})
export class EditableTextComponent {
  @Input('value') value: PropertyType;
  @Input('placeholder') placeholder: string = 'Click to edit';
  @Output('update') updateEmitter = new EventEmitter<PropertyType>();
  @Output('cancel') cancelEmitter = new EventEmitter<void>();
  formVisible: boolean = false;
  valueOrig: PropertyType;
  valueForm: PropertyType;

  get hasValue(): boolean {
    return this.value !== undefined
      && this.value !== null
      && this.value.toString().trim().length > 0;
  }

  onKeyup(ev: any) {
    if (ev.key === 'Enter') {
      this.save();
    } else if (ev.key === 'Escape') {
      this.cancel();
    }
  }

  showForm() {
    this.valueOrig = this.value;
    this.valueForm = this.value;
    this.formVisible = true;
  }

  save() {
    this.formVisible = false;

    this.value = this.valueForm;
    this.valueOrig = undefined;
    this.valueForm = undefined;

    this.updateEmitter.emit(this.value);
  }

  cancel() {
    this.formVisible = false;

    this.value = this.valueOrig;
    this.valueOrig = undefined;
    this.valueForm = undefined;

    this.cancelEmitter.emit();
  }

}
