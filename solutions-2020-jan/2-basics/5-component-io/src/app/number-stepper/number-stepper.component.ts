import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'number-stepper',
  templateUrl: './number-stepper.component.html',
  styleUrls: ['./number-stepper.component.css']
})
export class NumberStepperComponent {
  @Input('value') inp: number | string;
  @Output('valueChange') out = new EventEmitter<number>();

  dec() {
    this.resize(-1);
  }

  inc() {
    this.resize(+1);
  }

  resize(delta: number) {
    const nextValue = +this.inp + delta;
    const minValue = 1;
    const maxValue = 50;
    this.inp = Math.min(maxValue, Math.max(minValue, nextValue));
    this.out.emit(this.inp);
  }
}
