import { Component } from '@angular/core';
import { MultiplyService } from './services/multiply.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'multiply';
  value: number = 5;

  constructor(public mulSvc: MultiplyService){}
  
  onChange(ev:any) {
    console.debug('onChange', ev);
    console.debug('onChange, value', ev.target.value);
    this.mulSvc.factor = ev.target.value
  }

  onMouse(ev:any) {
    console.debug('onMouse', ev);
  }
}
