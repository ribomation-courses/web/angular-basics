import {Component, OnInit} from '@angular/core';
import {ProductsService}   from './services/products.service';
import {Observable}        from 'rxjs';
import {Product}           from './domain/product';

@Component({
    selector:    'app-root',
    templateUrl: './app.component.html',
    styleUrls:   ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'products-app';
    products$: Observable<Product[]>;
    product$: Observable<Product>;

    constructor(private productsSvc: ProductsService) {
    }

    ngOnInit(): void {
        this.products$ = this.productsSvc.findAll();
    }

    showProduct(id: string | number) {
        this.product$ = this.productsSvc.findById(+id);
    }
}
