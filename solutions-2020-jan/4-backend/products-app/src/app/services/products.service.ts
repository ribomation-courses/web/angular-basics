import {Injectable}     from '@angular/core';
import {HttpClient}     from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Product}        from '../domain/product';
import {catchError}     from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ProductsService {
    readonly baseUrl = 'http://localhost:3000/products';

    constructor(private httpSvc: HttpClient) {
    }

    findAll(): Observable<Product[]> {
        return this.httpSvc.get<Product[]>(this.baseUrl);
    }

    findById(productId: number): Observable<Product> {
        return this.httpSvc
            .get<Product>(this.baseUrl + '/' + productId)
            .pipe(
                catchError(err => {
                    //console.log('*** error', err);
                    return of({} as Product);
                })
            )
            ;
    }
}
