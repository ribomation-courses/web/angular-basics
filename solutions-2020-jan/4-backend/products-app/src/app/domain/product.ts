// {
//     "id": 1,
//     "name": "Salmon - Atlantic, No Skin",
//     "price": 606.75,
//     "date": "2019-08-07T11:06:48Z"
//   }

export interface Product {
    id: number;
    name: string;
    price: number;
    date: Date
}
