import {Component} from '@angular/core';
import {User}      from './domain/user';

@Component({
    selector:    'app-root',
    templateUrl: './app.component.html',
    styleUrls:   ['./app.component.scss']
})
export class AppComponent {
    user: User = {
        name: '',
        age:  0
    };

    onSubmitForm(payload: any) {
        console.log('form payload', payload);
    }

    onSelected(item: any) {
        console.log('Selected item', item);
    }

    products = [
        {name:'Apple'},
        {name:'Banana'},
        {name:'Coco Nut'},
        {name:'Date Plum'},
        {name:'Pine Apple'},
        {name:'Orange'},
        {name:'Pear'},
        {name:'Cucumber'},
    ];
}
