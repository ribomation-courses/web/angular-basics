import {
    Component,
    OnInit,
    EventEmitter,
    Input,
    Output
}                       from '@angular/core';
import {FormControl}    from '@angular/forms';
import {Observable, of} from 'rxjs';
import {
    debounceTime,
    distinctUntilChanged,
    map,
    switchMap
}                       from 'rxjs/operators';

@Component({
    selector:    'interactive-form',
    templateUrl: './interactive-form.component.html',
    styleUrls:   ['./interactive-form.component.scss']
})
export class InteractiveFormComponent implements OnInit {
    @Input('searchable') searchable: any[];
    @Output('selected') selectedEmitter = new EventEmitter<any>();
    results$: Observable<string[]>;

    phrase: FormControl = new FormControl();

    ngOnInit() {
        if (!this.searchable || this.searchable.length === 0) {
            throw new Error('empty searchable');
        }

        this.results$ = this.phrase.valueChanges
            .pipe(
                debounceTime(500),
                distinctUntilChanged(),
                map(txt => txt.toLowerCase()),
                switchMap(txt => this.fakeSearch(txt))
            );
    }

    onClicked(item: any) {
        this.selectedEmitter.emit(item);
    }

    private fakeSearch(phrase: string): Observable<string[]> {
        if (!phrase || phrase.trim().length === 0) {
            return of([]);
        }

        const result = this.searchable.filter(item => {
            return (item.name as string).toLowerCase().includes(phrase);
        });
        //console.log('*** fakeSearch "%s" %o', phrase, result);
        return of(result);
    }

}
