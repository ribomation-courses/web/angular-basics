import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {User}                                                      from '../../domain/user';
import {FormGroup}                                                 from '@angular/forms';

@Component({
    selector:    'template-oriented-form',
    templateUrl: './template-oriented-form.component.html',
    styleUrls:   ['./template-oriented-form.component.scss']
})
export class TemplateOrientedFormComponent implements OnInit {
    @Input('user') user: User;
    @Output('submit') submitEmitter = new EventEmitter<User>();
    @ViewChild('form', {static: true}) form: FormGroup;

    ngOnInit() {
        if (!this.user) {
            throw new Error('no user object provided');
        }
    }

    onSubmit() {
        if (this.form.valid) {
            this.submitEmitter.emit(this.form.value);
        }
    }

}
