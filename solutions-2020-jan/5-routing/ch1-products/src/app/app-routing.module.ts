import {NgModule}             from '@angular/core';
import {Route, RouterModule}  from '@angular/router';
import {WelcomeComponent}     from './pages/welcome/welcome.component';
import {ProductListComponent} from './pages/product-list/product-list.component';
import {NotFoundComponent}    from './pages/not-found/not-found.component';
import {ProductViewComponent} from './pages/product-view/product-view.component';

const routes: Route[] = [
    {path: 'home', component: WelcomeComponent},
    {path: 'product-list', component: ProductListComponent},
    {path: 'product/:productId', component: ProductViewComponent},
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: '**', component: NotFoundComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
