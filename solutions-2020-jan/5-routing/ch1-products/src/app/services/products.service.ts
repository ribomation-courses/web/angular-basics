import {Injectable}                   from '@angular/core';
import {HttpClient}                   from '@angular/common/http';
import {Observable, of}               from 'rxjs';
import {Product}                      from '../domain/product';
import {catchError, filter, map, tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ProductsService {
    readonly baseUrl            = 'http://localhost:3000/products';
    _simulateEmptyList: boolean = false;

    constructor(private httpSvc: HttpClient) {
    }

    set empty(val: boolean) {
        this._simulateEmptyList = val;
    }

    findAll(): Observable<Product[]> {
        return this.httpSvc
            .get<Product[]>(this.baseUrl)
            .pipe(
                map(objs => {
                    if (this._simulateEmptyList) {
                        return [];
                    } else {
                        return objs;
                    }
                })
            );
    }

    findById(productId: number): Observable<Product> {
        return this.httpSvc
            .get<Product>(this.baseUrl + '/' + productId)
            .pipe(
                catchError(err => {
                    return of({} as Product);
                })
            )
            ;
    }
}
