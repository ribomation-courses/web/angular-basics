import {BrowserModule} from '@angular/platform-browser';
import {NgModule}      from '@angular/core';

import {AppRoutingModule}     from './app-routing.module';
import {AppComponent}         from './app.component';
import {WelcomeComponent}     from './pages/welcome/welcome.component';
import {ProductListComponent} from './pages/product-list/product-list.component';
import {ProductViewComponent} from './pages/product-view/product-view.component';
import {NotFoundComponent}    from './pages/not-found/not-found.component';
import {HttpClientModule}     from '@angular/common/http';
import {FormsModule}          from '@angular/forms';

@NgModule({
    declarations: [
        AppComponent,
        WelcomeComponent,
        ProductListComponent,
        ProductViewComponent,
        NotFoundComponent
    ],
    imports:      [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        AppRoutingModule
    ],
    providers:    [],
    bootstrap:    [AppComponent]
})
export class AppModule {}
