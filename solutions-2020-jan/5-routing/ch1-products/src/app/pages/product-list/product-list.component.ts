import {Component, OnInit} from '@angular/core';
import {ProductsService}   from '../../services/products.service';
import {Observable}        from 'rxjs';
import {Product}           from '../../domain/product';
import {Router}            from '@angular/router';

@Component({
    selector:    'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls:   ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
    list$: Observable<Product[]>;

    constructor(
        private productsSvc: ProductsService,
        private routerSvc: Router
    ) {
    }

    ngOnInit() {
        this.list$ = this.productsSvc.findAll();
    }

    gotoProduct(id: number) {
        console.log('goto', id);
        this.routerSvc.navigate(['/product', id]);
    }
}
