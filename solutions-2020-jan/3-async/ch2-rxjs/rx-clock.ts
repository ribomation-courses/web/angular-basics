import {interval} from 'rxjs';
import {tap, filter, map, take} from 'rxjs/operators';

const N = +(process.argv[2] || 5);
const now = () => new Date().toLocaleTimeString();

interval(250)
.pipe(
    filter(x => x % 4 === 0),
    map(y => new Date()),
    map(d => d.toLocaleString()),
    take(N),
    //tap(x => console.log(now(), 'x=', x))
)
.subscribe(
    t => console.log(t), 
    _ => {}, 
    () => console.log('DONE')
    );
