import { Account } from './account';
import { Generator } from './generator';

const g = new Generator();
const N = 5;
const accounts = [];
for (let k = 1; k <= N; ++k) accounts.push(g.nextAccount());

for (let k = 0; k < accounts.length; ++k) console.log(accounts[k].toString());
console.log('---');
console.log('accounts[0]:', accounts[0].toString());
console.log('accounts[0].accountNumber:', accounts[0].accountNumber);
console.log('accounts[0].balance:', accounts[0].currentBalance);

accounts[0].currentBalance = 500;
console.log('accounts[0]:', accounts[0].toString());

try {
    accounts[0].currentBalance = -500;
} catch (err) {
    console.log('*** error:', err.message);
}
