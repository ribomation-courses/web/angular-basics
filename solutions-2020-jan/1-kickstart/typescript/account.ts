export class Account {
    constructor(
        private accno: string,
        private balance: number,
        private credit?: boolean,
        private created?: Date,
        private owner?: string
    ) {
        if (!this.created) this.created = new Date();
        if (!this.owner) this.owner = 'Anybody';
        if (!this.credit) this.credit = true;
    }

    get accountNumber(): string { return this.accno; }

    get currentBalance(): number { return this.balance; }
    set currentBalance(val: number) {
        if (val < 0) throw new Error('negative');
        this.balance = val;
    }

    toString(): string {
        return `Account<${this.accno}, ${this.balance} kr, credit:${this.credit}, created:${this.created.toLocaleTimeString()}, owner:${this.owner}>`;
    }

}

