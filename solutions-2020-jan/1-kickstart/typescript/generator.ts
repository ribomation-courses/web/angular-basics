import { Account } from './account';

export class Generator {
    banks: string[] = ['HB', 'SEB', 'Swedbank'];
    names: string[] = ['Anna', 'Berit', 'Carin'];

    nextInt(lb: number, ub: number): number {
        if (ub <= lb) throw new Error('invalid number range');
        const interval = ub - lb + 1;
        return Math.floor(Math.random() * interval + lb);
    }

    nextInts(n: number, lb: number, ub: number): string {
        let result = '';
        while (n-- > 0) result += this.nextInt(lb, ub).toString(10);
        return result;
    }

    nextElem(arr: string[]): string {
        return arr[this.nextInt(0, arr.length - 1)];
    }

    nextAccno() {
        return this.nextElem(this.banks) + this.nextInts(6, 1, 9);
      }

    nextAccount(): Account {
        return new Account(this.nextAccno(), this.nextInt(100, 1000));
    }
}
