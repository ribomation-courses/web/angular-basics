import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector:    'app-operands',
  templateUrl: './operands.component.html',
  styleUrls:   ['./operands.component.css']
})
export class OperandsComponent {
  @Output('lhs') lhsOut: EventEmitter<number> = new EventEmitter<number>();
  @Output('rhs') rhsOut: EventEmitter<number> = new EventEmitter<number>();

  lhs: number = 20;
  rhs: number = 25;

  compute(): void {
    console.info('[operands]', this.lhs, this.rhs);
    this.lhsOut.emit(this.lhs);
    this.rhsOut.emit(this.rhs);
  }
}
