import {Injectable} from '@angular/core';
import {Result}     from './result.model';

@Injectable(/*{
  providedIn: 'root'
}*/)
export class EvalService {
  compute(lhs: number, rhs: number): Result[] {
    return [
      this.add(lhs, rhs),
      this.mul(lhs, rhs),
      this.sub(lhs, rhs),
      this.div(lhs, rhs),
    ];
  }

  private add(lhs: number, rhs: number): Result {
    return new Result('addition', '+', lhs, rhs, lhs + rhs);
  }

  private mul(lhs: number, rhs: number): Result {
    return new Result('multiplication', '*', lhs, rhs, lhs * rhs);
  }

  private sub(lhs: number, rhs: number): Result {
    return new Result('subtraction', '-', lhs, rhs, lhs - rhs);
  }

  private div(lhs: number, rhs: number): Result {
    return new Result('division', '/', lhs, rhs, lhs / rhs);
  }

}
