
class Book {
    constructor(
        private _title : string,
        private _author: string,
        private _price : number,
    ){}

    get title() : string { return this._title.toUpperCase(); }
    get author(): string { return this._author.toUpperCase(); }
    get price() : number { return this._price * 1.25; }
}


const b = new Book('ng primer', 'Harry Hacker', 420);
console.log(`b = Book{title:${b.title}, author:${b.author}, price:${b.price}kr}`);

