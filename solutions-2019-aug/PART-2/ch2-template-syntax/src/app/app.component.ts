import {Component} from '@angular/core';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styles:      [`
    #target {
      color: orangered;
      margin-top: 1rem;
    }
    h2 {
      background-color: lightgray;
    }
    number-stepper {
      border: solid thin gray;
      padding: 0.5rem;
    }
  `]
})
export class AppComponent {
  obj1: any = {
    name: {
      fname: 'Nisse'
    }
  };
  obj2: any = {};

  fontSize: number = 3;
}
