import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { KrPipe } from './kr.pipe';
import { BoolPipe } from './bool.pipe';
import { NbspPipe } from './nbsp.pipe';

@NgModule({
  declarations: [
    AppComponent,
    KrPipe,
    BoolPipe,
    NbspPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
