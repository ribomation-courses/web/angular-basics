export class Product {
  name: string         = '';
  price: number        = 0;
  isService: boolean   = false;
  itemsInStore: number = 0;
  lastUpdated: Date    = new Date();
}

