import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'nbsp'
})
export class NbspPipe implements PipeTransform {
  transform(value: string): any {
    if (!value) {return '';}
    return value.replace(/\s+/g, '\xA0');
  }
}
