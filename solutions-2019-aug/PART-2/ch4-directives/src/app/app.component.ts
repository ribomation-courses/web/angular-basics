import {Component} from '@angular/core';
import {Vehicle}   from './vehicle.model';
import {Generator} from './generator.model';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent {
  count: number       = 5;
  vehicles: Vehicle[] = [];

  constructor() {
    this.update();
  }

  update(): void {
    this.vehicles = Generator.generate(this.count);
  }

  get hasVehicles(): boolean {
    return this.vehicles.length > 0;
  }

  shuffle() {
    this.count = Math.floor(10 * Math.random());
    this.update();
  }


}
