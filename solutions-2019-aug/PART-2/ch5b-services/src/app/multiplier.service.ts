import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MultiplierService {
  private _factor: number = 10;

  multiply(n: number): number {
    return n * this._factor;
  }

  set factor(value: number) {
    this._factor = value;
  }

  get factor(): number {
    return this._factor;
  }
}
