import {Component}         from '@angular/core';
import {MultiplierService} from './multiplier.service';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent {
  value: number  = 42;
  result: number = 10;

  constructor(private multSvc: MultiplierService) {
    this.update();
  }

  update(): void {
    this.result = this.multSvc.multiply(this.value);
  }

  setFactor(value: number | string) {
    console.info('setFactor', value);
    this.multSvc.factor = +value;
    this.update();
  }

}
