import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent }            from './app.component';
import { CalendarWidgetComponent } from './calendar-widget/calendar-widget.component';
import {FormsModule}               from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    CalendarWidgetComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
