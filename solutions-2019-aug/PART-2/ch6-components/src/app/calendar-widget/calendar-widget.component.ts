import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

interface Day {
  date: number;
  marked?: boolean;
}

interface Week {
  days: Day[];
}


@Component({
  selector:    'calendar-widget',
  templateUrl: './calendar-widget.component.html',
  styleUrls:   ['./calendar-widget.component.scss']
})
export class CalendarWidgetComponent implements OnInit, OnChanges {
  @Input('from') fromDate: Date | string;
  @Input('days') numDays: number = 1;
  year: number;
  month: string;
  weeks: Week[];

  constructor() {
  }

  ngOnInit() {
    if (!this.fromDate) {
      throw new Error('[calendar-widget] missing date');
    }
    if (this.numDays < 1 || 5 < this.numDays) {
      throw new Error('[calendar-widget] days must be within [1,5]');
    }

    this.updateView();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.updateView();
  }

  private updateView() {
    const date: Date    = (typeof this.fromDate === 'string')
      ? new Date(this.fromDate)
      : this.fromDate as Date;
    const year          = date.getFullYear();
    const monthIndex    = date.getMonth();
    const daysOfMonth   = this.numDaysOfMonth(year, monthIndex);
    const firstDayIndex = this.firstDayIndex(year, monthIndex);

    this.year  = year;
    this.month = this.monthNames[monthIndex];
    this.weeks = this.generateWeeks(daysOfMonth, firstDayIndex, date.getDate(), this.numDays);
  }

  private generateWeeks(daysOfMonth: number, firstDayIndex: number,
                        dateMarked: number, daysMarked: number): Week[] {
    const weeks: Week[] = [];
    let date: number    = 1;
    for (let row = 1; row <= 6; ++row) {
      let week: Day[] = [];
      for (let day = 1; day <= 7; ++day) {
        if ((day < firstDayIndex && date === 1)) { // prev month
          week.push({date: 0});
        } else if (daysOfMonth < date) { // next month
          const dateNext: number = date - daysOfMonth;
          week.push({date: dateNext, marked: (date === dateMarked)});
          if (date === dateMarked && daysMarked > 1) {
            --daysMarked;
            ++dateMarked;
          }
          ++date;
        } else {
          week.push({date: date, marked: (date === dateMarked)});
          if (date === dateMarked && daysMarked > 1) {
            --daysMarked;
            ++dateMarked;
          }
          ++date;
        }
      }
      weeks.push({days: week});
      if (daysOfMonth < date) {
        break;
      }
    }
    return weeks;
  }

  private numDaysOfMonth(year: number, monthIdx: number): number {
    return 32 - new Date(year, monthIdx, 32).getDate();
  }

  private firstDayIndex(year: number, monthIdx: number): number {
    return (new Date(year, monthIdx)).getDay();
  }


  readonly weekDays: string[] = [
    'Må', 'Ti', 'On', 'To', 'Fr', 'Lö', 'Sö'
  ];

  readonly monthNames: string[] = [
    'Januari', 'Februari', 'Mars', 'April', 'Maj', 'Juni',
    'Juli', 'Augusti', 'September', 'Oktober', 'November', 'December'
  ];

  onDayClicked(day: Day) {
    console.warn('clicked', day)
  }
}
