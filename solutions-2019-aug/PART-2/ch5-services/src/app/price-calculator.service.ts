import {Inject, Injectable}         from '@angular/core';
import {SHIPPING_COST, VAT_PERCENT} from './tokens';

@Injectable({
  providedIn: 'root'
})
export class PriceCalculatorService {
  constructor(
    @Inject(VAT_PERCENT) private vatPercent: number,
    @Inject(SHIPPING_COST) private shippingCost: number,
  ) {
  }

  get VAT(): number {
    return this.vatPercent / 100;
  }

  get shipping(): number {
    return this.shippingCost;
  }

  vatAmountOf(amount: number): number {
    return amount * this.VAT;
  }

  totalPrice(amount: number): number {
    const total = amount + this.vatAmountOf(amount) + this.shipping;
    return Math.floor(total);
  }

}
