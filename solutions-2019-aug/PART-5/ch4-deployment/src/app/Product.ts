export class Book {
  id: number;
  name: string;
  pages: number;
  price: number;
  itemsInStore: number;
  lastUpdated: Date;
}

function randInt(lb: number, ub: number): number {
  return Math.floor(lb + (ub - lb + 1) * Math.random());
}

const names: string[] = [
  'Angular Super Book', 'TypeScript Ultra Book', 'JavaScript for Hackers',
  'NodeJS Ultimate Bible', 'REST Web Services with ExpressJS',
  'NPM Kick Start', 'Up and running with Bower', ' GulpJS Masters',
  'Twitter Bootstrap Ninja', 'Mastering FontAwesome'
];

const DAYS: number = 24 * 3600 * 1000;

export class Generator {
  private static nextBookIndex = 0;
  private static _books: Book[] = [];

  static mk(): Book {
    let p: Book = new Book();
    p.id = this.nextBookIndex++;
    p.name = names[p.id % names.length];
    p.pages = randInt(100, 1000);
    p.price = randInt(100, 500);
    p.itemsInStore = randInt(0, 10);
    p.lastUpdated = new Date(Date.now() - randInt(1, 45) * DAYS);
    return p;
  }

  static generate(n: number): void {
    this._books = [];
    for (let k = 0; k < n; ++k) this._books.push(this.mk());
  }

  static books(): Book[] {
    if (this._books.length ===0) {
      this.generate(10);
    }
    return this._books;
  }

  static book(id: number): Book {
    if (0 <= id && id < this._books.length) {
      return this._books[id];
    }
    console.warn('book gen', 'invalid id', id);
  }

}
