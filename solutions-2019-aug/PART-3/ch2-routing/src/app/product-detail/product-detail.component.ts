import {Component, OnDestroy, OnInit} from '@angular/core';
import {map}                          from 'rxjs/operators';
import {Book}                         from '../book';
import {ActivatedRoute}               from '@angular/router';
import {BookRepo}                     from '../book-repo';
import {Observable, Subscription}     from 'rxjs';

@Component({
  selector:    'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls:   ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit, OnDestroy {
  book$: Observable<Book>;
  sub: Subscription;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.book$ = this.route
      .params
      .pipe(
        map(p => p.id),
        map(id => BookRepo.book(id))
      );

    //this.sub = this.book$.subscribe();
  }

  ngOnDestroy(): void {
    //if (this.sub) { this.sub.unsubscribe(); }
  }

}
