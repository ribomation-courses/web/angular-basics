import { Component, OnInit } from '@angular/core';
import {Router}              from '@angular/router';
import {Book}                from '../book';
import {BookRepo}            from '../book-repo';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styles: []
})
export class ProductListComponent implements OnInit {
  books: Book[] = [];

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.books = BookRepo.books();
  }

  view(id: number): void {
    this.router.navigate(['/products/detail', id]);
  }
}
