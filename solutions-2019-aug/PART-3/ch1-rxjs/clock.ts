import {interval} from 'rxjs';
import {map, debounceTime, filter, throttleTime} from 'rxjs/operators';

const clock$ = 
	interval(250)
	.pipe(
		map(_ => Date.now()),
		map(ts => Math.floor(ts/1000)),
		throttleTime(1000),
		filter(ts => (ts % 3) === 0),
		map(_ => new Date().toISOString()),
		map(ts => ts.replace('T', ' ')),
		map(ts => ts.substr(0, 19))
	)
	.subscribe(x => console.log('**', x));
