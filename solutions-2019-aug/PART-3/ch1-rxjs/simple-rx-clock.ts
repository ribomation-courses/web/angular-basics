import { interval } from 'rxjs';
import { map, filter, take, tap } from 'rxjs/operators';

interval(250)
    .pipe(
        filter(n => (n + 1) % 4 === 0),
        map(n => new Date()),
        map(d => d.toLocaleTimeString()),
        take(10)
    )
    .subscribe(value => console.info('#', value))
    ;
