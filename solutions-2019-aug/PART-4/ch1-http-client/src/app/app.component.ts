import {Component, OnInit} from '@angular/core';
import {UsersService}      from './users.service';
import {Observable}        from 'rxjs';
import {User}              from './user';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent implements OnInit {
  users$: Observable<User[]>;
  user$: Observable<User>;
  userFormData: User;

  constructor(private usersSvc: UsersService) {
  }

  ngOnInit(): void {
    this.users$ = this.usersSvc.findAll();
    this.user$  = this.usersSvc.findAny();
  }

  edit(user: User) {
    this.userFormData = user;
  }

  create() {
    this.userFormData = {name: '', age: 0};
  }

  save() {
    if (this.userFormData) {
      const done = () => {
        this.userFormData = undefined;
        this.ngOnInit();
      };

      if (this.userFormData.id) {
        this.usersSvc.update(this.userFormData)
          .subscribe(done);
      } else {
        this.usersSvc.create({name: this.userFormData.name, age: this.userFormData.age})
          .subscribe(done);
      }
    }
  }

  delete(user: User) {
    this.usersSvc.remove(user).subscribe(_ => {
      this.ngOnInit();
    });
  }

}
