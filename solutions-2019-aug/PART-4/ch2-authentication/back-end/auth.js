'use strict';
const JWT          = require('njwt');
const genRandomKey = require('secure-random');

const KEY = genRandomKey(256, {type: 'Buffer'});
console.log('[key]', KEY.toString('base64'));


function isUserDefined(usr, pwd) {
  return usr === 'nisse' && pwd === 'hult';
}

function login(body) {
  console.log('login', body);
  const usr = body.username;
  const pwd = body.password;
  if (!isUserDefined(usr, pwd)) return false;

  const claims = {
    iss: 'http://localhost:5000/api/users',
    sub: usr
  };
  const token  = JWT.create(claims, KEY);
  console.log('token', token);
  return token.compact();
}

function isAuthenticated(req) {
  const token = req.get('Auth-Token');
  if (token !== undefined) {
    try {
      JWT.verify(token, KEY);
      return true;
    } catch (x) {
    }
  }
  return false;
}


function isLogin(req) {
  return req.method === 'POST' && req.originalUrl === '/auth/login';
}

function authRequired(req) {
  const OP = req.method;
  return ['POST', 'PUT', 'DELETE'].some(x => x === OP);
}

module.exports = (req, res, nxt) => {
  if (isLogin(req)) {
    const token = login(req.body);
    if (token) {
      res.status(201).send({
        token: token
      });
    } else {
      res.sendStatus(401);
    }
  } else if (authRequired(req)) {
    if (isAuthenticated(req)) {
      nxt();
    } else {
      res.sendStatus(401);
    }
  } else {
    nxt();
  }
};
